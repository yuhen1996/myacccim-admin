import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { MenWomenCategoryPopOverPage } from '../pages/men-women-category-pop-over/men-women-category-pop-over';
import { BagPage } from '../pages/bag/bag';
import { SavedItemsPage } from '../pages/saved-items/saved-items';
import { AccountPage } from '../pages/account/account';
import { AppSettingsPage } from '../pages/app-settings/app-settings';
import { HelpFaqPage } from '../pages/help-faq/help-faq';
import { SavedItemsPageModule } from '../pages/saved-items/saved-items.module';
import { RecommendedPopOverPage } from '../pages/recommended-pop-over/recommended-pop-over';
import { FilterPage } from '../pages/filter/filter';
import { ProductSizePopOverPage } from '../pages/product-size-pop-over/product-size-pop-over';
import { ProductColourPopOverPage } from '../pages/product-colour-pop-over/product-colour-pop-over';
import { LoginPage } from '../pages/login/login';
import { ApiserviceProvider } from '../providers/apiservice/apiservice';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { ProductDetailsPage } from '../pages/product-details/product-details';
import { CommitteeDetailPage } from '../pages/committee-detail/committee-detail';
import { QrScanPage } from '../pages/qr-scan/qr-scan';
import { BarcodeScanner} from '@ionic-native/barcode-scanner';
import { CallNumber } from '@ionic-native/call-number/ngx'
import { AttandancelistPage} from '../pages/attandancelist/attandancelist';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    MenWomenCategoryPopOverPage,
    RecommendedPopOverPage,
    ProductSizePopOverPage,
    ProductColourPopOverPage,
    BagPage,
    AccountPage,
    AppSettingsPage,
    HelpFaqPage,
    FilterPage,
    LoginPage,
    ProductDetailsPage,
    CommitteeDetailPage,
    QrScanPage,
    AttandancelistPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    SavedItemsPageModule,
    IonicStorageModule.forRoot({ name: '__mydb',
    driverOrder: ['indexeddb', 'sqlite', 'websql']}),
    HttpModule,
    HttpClientModule
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    MenWomenCategoryPopOverPage,
    RecommendedPopOverPage,
    ProductSizePopOverPage,
    ProductColourPopOverPage,
    BagPage,
    SavedItemsPage,
    AccountPage,
    AppSettingsPage,
    HelpFaqPage,
    FilterPage,
    LoginPage,
    ProductDetailsPage,
    CommitteeDetailPage,
    QrScanPage,
    AttandancelistPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiserviceProvider,
    BarcodeScanner,
    CallNumber
    
    
   
    
  ]
})
export class AppModule {}
