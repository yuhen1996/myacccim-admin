import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError,tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { Http, Response } from '@angular/http';

/*
  Generated class for the ApiserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
const httpOptions = {
  headers : new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class ApiserviceProvider {

  public AdminLoginAPI = "https://api.acccim-registration.org.my/api/AdminSignIn?id=";
  public ShowAllMember = "https://api.acccim-registration.org.my/api/DisplayOnlyMember";
  public ShowAllNonMember = "https://api.acccim-registration.org.my/api/DisplayOnlyNonMember";
  public ShowAllLocalGuest = "http://api.acccim-registration.org.my/api/DisplayOnlyLocalGuest";
  public ShowAllOverseaGuest = "http://api.acccim-registration.org.my/api/DisplayOnlyOverseaGuest";
  public getUserData = "https://api.acccim-registration.org.my/api/UserRegistrationData?id=";
  public TotalMember = "https://api.acccim-registration.org.my/api/TotalMember";
  public TotalNonMember = "https://api.acccim-registration.org.my/api/TotalNonMember";
  public TotalLocalGuest = "https://api.acccim-registration.org.my/api/TotalLocalGuest";
  public TotalOverseaGuest = "https://api.acccim-registration.org.my/api/TotalOverseaGuest";
  public TotalDiamondSponsor = "https://api.acccim-registration.org.my/api/TotalDiamondSponsors";
  public TotalPlatinumSponsor = "https://api.acccim-registration.org.my/api/TotalPlatinumSponsors";
  public TotalGoldSponsor = "https://api.acccim-registration.org.my/api/TotalGoldSponsors";
  public TotalSilverSponsor = "https://api.acccim-registration.org.my/api/TotalSilverSponsors";
  public TotalSupportingSponsor = "https://api.acccim-registration.org.my/api/TotalSupportingSponsors";
  public TotalEvent = "https://api.acccim-registration.org.my/api/TotalEvent";
  public eventListing = "https://api.acccim-registration.org.my/api/EventListing";
  public specificevent = "https://api.acccim-registration.org.my/api/SpecificEvent?eventid=";
  public rsvpeventlist = "https://api.acccim-registration.org.my/api/RsvpEventList?Id=";
  public scanattendance = "https://api.acccim-registration.org.my/api/Attendance?RegistrationID=";
  public presentattendance = "https://api.acccim-registration.org.my/api/PresentAttendance?Id=";
  public absentattendance = "https://api.acccim-registration.org.my/api/PendingAbsentAttendance?Id="
  public getAdminData = "https://api.acccim-registration.org.my/api/getAdmin?id=";
  constructor( public http: HttpClient, 
               public https: Http) {
   

  }


  //Scan Attendance 
  QRScanAttendance(userid, eventid)
  {
    var uid = userid;
    var eid = eventid;
    return this.https.put(this.scanattendance + uid + '&&EventID=' + eid, httpOptions).pipe();
    
    
  }


//User Login
  adminLogin(user,pass)
  {
    var id = user;
    var pw = pass;

    if(id != null && pw != null)
    {
      return this.https.get(this.AdminLoginAPI + id + '&&pw=' + pw);
    }
  }

  getAllMember()
  {
    return this.https.get(this.ShowAllMember);
  }

  getAllNonMember()
  {
    return this.https.get(this.ShowAllNonMember);
  }

  getAllLocalGuest()
  {
    return this.https.get(this.ShowAllLocalGuest);
  }

  getAllOverseaGuest()
  {
    return this.https.get(this.ShowAllOverseaGuest);
  }

  getUser(id)
  {
   var userid = id
    return this.https.get(this.getUserData + userid );
  }

  /*Admin Dashboard*/
  getTotalMember()
  {
    return this.https.get(this.TotalMember);
  }

  getTotalNonMember()
  {
    return this.https.get(this.TotalNonMember);
  }

  getTotalEvent()
  {
    return this.https.get(this.TotalEvent);
  }

  getLocalGuest()
  {
    return this.https.get(this.TotalLocalGuest);
  }

  getOverseaGuest()
  {
    return this.https.get(this.TotalOverseaGuest);
  }

  getDiamondSponsor()
  {
    return this.https.get(this.TotalDiamondSponsor);
  }

  getPlatinumSponsor()
  {
    return this.https.get(this.TotalPlatinumSponsor);
  }

  getGoldSponsor()
  {
    return this.https.get(this.TotalGoldSponsor);
  }

  getSilverSponsor()
  {
    return this.https.get(this.TotalSilverSponsor);
  }

  getSupportingSponsor()
  {
    return this.https.get(this.TotalSupportingSponsor);
  }

  getEvent()
  {
    return this.https.get(this.eventListing);
  }

  getSpecificEvent(evtid)
  {
    var eventid = evtid;
    
    return this.https.get(this.specificevent + eventid);
  }

  getRsvpEventList(eventID)
  {
    var eid = eventID;
    return this.https.get(this.rsvpeventlist + eid);
  }

  PresentAttendance(id)
  {
    return this.https.get(this.presentattendance + id);
  }

  AbsentAttendance(id)
  {
    return this.https.get(this.absentattendance + id);
  }

  getAdmin(id)
  {
   var userid = id
    return this.https.get(this.getAdminData + userid );
  }

}
