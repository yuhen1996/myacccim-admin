import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ApiserviceProvider } from '../../providers/apiservice/apiservice';
import {Http,Response} from '@angular/http';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Renderer } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { CommitteeDetailPage } from '../committee-detail/committee-detail';
/**
 * Generated class for the QrScanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-qr-scan',
  templateUrl: 'qr-scan.html',
})
export class QrScanPage {

  scanqrinfo: any = "scan";
  EventID: any;
  showRsvpEvent: any;
  scannedCode = null;
  items: any;
  items2: any;
  showSpecUser: any;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public ApiService: ApiserviceProvider,
              public barcodeScanner: BarcodeScanner,
              public renderer: Renderer,
              public call : CallNumber,
              public alertCtrl: AlertController) {

    this.EventID = navParams.get('data');
    this.attendanceRefresh(0);
  }

  
  scanCode(){
   
    this.barcodeScanner.scan().then(barcodeData => {
      this.scannedCode = barcodeData.text;
      this.ApiService.QRScanAttendance(this.scannedCode , this.EventID)
      .subscribe(result => {
        let alert = this.alertCtrl.create({
          title: 'Successfully Scan',
          subTitle: 'This user has been attend the event.',
          buttons: ['Ok']
        });
        alert.present();

        
      },
      (err:Error) => {
        
        //console.log("Failed");
        let alert = this.alertCtrl.create({
          title: 'Failed To Scan',
          subTitle: 'Hi, you are either checked in or not registered for this programme. Kindly see our person in-charge for further assistance.',
          buttons: ['Ok']
        });
        alert.present();
 
    });
    });

    
}

attendanceRefresh(refresher){

  this.ApiService.getRsvpEventList(this.EventID)
    .subscribe((res:Response) => {
      this.items = res.json();
      this.initializeAttendance();
       if(refresher != 0)
       {
        refresher.complete();
       }
     
    })

  }
  
getUserUpdate(usr:any)
{
  this.initializeAttendance();
  
  let serVal = usr.target.value;
  if(serVal && serVal.trim() != '')
  {
    
    this.showRsvpEvent = this.showRsvpEvent.filter((getuser) => {
      return(getuser.MemberName.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
     
    });
    this.renderer.invokeElementMethod(usr.target, 'blur');
    
  }
}

initializeAttendance() {
  this.showRsvpEvent = this.items;
}


showSpecificUser(userid){
  this.ApiService.getUser(userid).subscribe((res:Response) => {
    const getSpecificUser = res.json();
    this.showSpecUser = getSpecificUser;

    this.navCtrl.push(CommitteeDetailPage,{
      data: userid
      
    });
   
    
  });


}
}
