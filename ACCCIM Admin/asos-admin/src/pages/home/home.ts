import { Component } from '@angular/core';
import { NavController, PopoverController, Events, LoadingController,AlertController } from 'ionic-angular';
import { MenWomenCategoryPopOverPage } from '../men-women-category-pop-over/men-women-category-pop-over';
import { CategoryPage, Category } from '../category/category';
import { ApiserviceProvider } from '../../providers/apiservice/apiservice';
import {Http,Response} from '@angular/http';
import { timestamp } from 'rxjs/operator/timestamp';
import { Renderer } from '@angular/core';
import { ProductDetailsPage } from '../product-details/product-details';
import { CommitteeDetailPage } from '../committee-detail/committee-detail';
import { P } from '@angular/core/src/render3';
import { QrScanPage } from '../qr-scan/qr-scan';
import { AttandancelistPage } from '../attandancelist/attandancelist';
import {Storage} from "@ionic/storage";
import { LoginPage } from '../login/login';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  homeSegment: string = "home";
  committeeinfo: any = "member";
  
  showMember: any;
  showNonMember: any;
  showLocalGuest: any;
  showOverseaGuest: any;

  memberitems : any;
  nonmemberitems: any;
  localguestitems: any;
  overseaguestitems: any;
  showSpecUser : any;

  totalmember: any;
  totalnonmember: any;
  totallocalguest: any;
  totaloverseaguest: any;

  totalevent : any; 

  totaldiamondsponsor: any;
  totalplatinumsponsor: any;
  totalgoldsponsor: any;
  totalsilversponsor:any;
  totalsupportingsponsor:any;

  eventlisting: any;
  items: any;

  showSpecificEvent: any;
  eventlistingattendance: any;
  eventattendanceitems: any;
  id: any;
  userCredential:any;
  userislogin: any;
  constructor(public navCtrl: NavController, 
              public popoverCtrl: PopoverController, 
              public events: Events,
              public ApiService:ApiserviceProvider,
              public renderer : Renderer,
              public loading: LoadingController,
              public alertCtrl: AlertController,
              public storage: Storage) {
               
                this.allRefresh(0);
               

                this.storage.get("userid").then((val) => {
                  this.id = val;
            
                  if(this.id === null)
                  {
                    this.navCtrl.setRoot(LoginPage);
            
                  }
            
                  else
                  {
                    this.ApiService.getAdmin(this.id)
                    .subscribe((res: Response) =>
                    {
                      
                      this.userCredential = res.json();
                     
                    });
                   
                    
                  }
                });
    
  }

  signOut()
  {
    let alert = this.alertCtrl.create({
      title: 'Are You Sure?',
      message: 'You will be Signed out.',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel');
          }
        },
        {
          text: 'Ok',
          handler: () => {
            this.storage.clear();
            this.presentLoadingWithOptions();
            //this.DisplaySuccessSignOutMessage();
            this.navCtrl.push(HomePage);
            
          }
        }
      ]
    });
    alert.present();

    
  }

  async presentLoadingWithOptions() {
    const loading = await this.loading.create({
      spinner: null,
      duration: 3000,
      content: 'Signing Out...',
      //translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    
    return await loading.present();
  }

 
  /*#region Committee*/
  allRefresh(refresher){

    //Member
    this.ApiService.getAllMember()
    .subscribe((res:Response) => {
      const getMember = res.json();
      this.memberitems = getMember;
     
      this.initializeMember();
      if(refresher != 0)
      {
       refresher.complete();
      }
    });

    //localguest
    this.ApiService.getAllLocalGuest()
    .subscribe((res:Response) => {
      const getLocalGuest = res.json();
      this.localguestitems = getLocalGuest;

      this.initializeLocalGuest();
      if(refresher != 0)
      {
       refresher.complete();
      }
    });

    //overseaguest
    this.ApiService.getAllOverseaGuest()
    .subscribe((res:Response) => {
      const getOverseaGuest = res.json();
      this.overseaguestitems = getOverseaGuest;
      this.initializeOverseaGuest();
      if(refresher != 0)
      {
       refresher.complete();
      }
    });

    //NonMember
    this.ApiService.getAllNonMember()
    .subscribe((res:Response) => {
      const getNonMember = res.json();
      this.nonmemberitems = getNonMember;

      this.initializeNonMember();
      if(refresher != 0)
      {
       refresher.complete();
      }
    });

    //Member
    this.ApiService.getTotalMember()
    .subscribe((res:Response)=> {
      const member = res.json();
      this.totalmember = member;

      if(refresher != 0)
         {
          setTimeout(() => {
            refresher.complete();
          }, 2000);
         }
    });

    this.ApiService.getTotalNonMember()
    .subscribe((res:Response) => {
      const nonmember = res.json();
      this.totalnonmember = nonmember;
      if(refresher != 0)
      {
       setTimeout(() => {
         refresher.complete();
       }, 2000);
      }
    });

    this.ApiService.getLocalGuest()
    .subscribe((res:Response) => {
      const localguest = res.json();
      this.totallocalguest = localguest;
      if(refresher != 0)
      {
       setTimeout(() => {
         refresher.complete();
       }, 2000);
      }
    });

    this.ApiService.getOverseaGuest()
    .subscribe((res:Response) => {
      const overseaguest = res.json();
      this.totaloverseaguest = overseaguest;
      if(refresher != 0)
      {
       setTimeout(() => {
         refresher.complete();
       }, 2000);
      }
    });

    this.ApiService.getTotalEvent()
    .subscribe((res:Response) => {
      const eventt = res.json();
      this.totalevent = eventt;

      if(refresher != 0)
      {
       setTimeout(() => {
         refresher.complete();
       }, 2000);
      }
    });

    this.ApiService.getDiamondSponsor()
    .subscribe((res:Response) => {
      const diamondsponsor = res.json();
      this.totaldiamondsponsor  = diamondsponsor;
      if(refresher != 0)
      {
       setTimeout(() => {
         refresher.complete();
       }, 2000);
      }
    });

    this.ApiService.getPlatinumSponsor()
    .subscribe((res:Response) => {
      const platinumsponsor = res.json();
      this.totalplatinumsponsor  = platinumsponsor;
      if(refresher != 0)
      {
       setTimeout(() => {
         refresher.complete();
       }, 2000);
      }
    });

    this.ApiService.getGoldSponsor()
    .subscribe((res:Response) => {
      const goldsponsor = res.json();
      this.totalgoldsponsor  = goldsponsor;

      if(refresher != 0)
      {
       setTimeout(() => {
         refresher.complete();
       }, 2000);
      }
    })

   
    this.ApiService.getSilverSponsor()
    .subscribe((res:Response) => {
      const silversponsor = res.json();
      this.totalsilversponsor  = silversponsor;
      if(refresher != 0)
      {
       setTimeout(() => {
         refresher.complete();
       }, 2000);
      }
    })
  
    this.ApiService.getSupportingSponsor()
    .subscribe((res:Response) => {
      const supportingsponsor = res.json();
      this.totalsupportingsponsor  = supportingsponsor;
      if(refresher != 0)
      {
       setTimeout(() => {
         refresher.complete();
       }, 2000);
      }
    })

    this.ApiService.getEvent().subscribe((res:Response) => {
      this.items = res.json();
      this.initializeEvent();
         if(refresher != 0)
         {
          refresher.complete();
         }

    });

    this.ApiService.getEvent().subscribe((res:Response) => {
      this.eventattendanceitems = res.json();
      this.initializeEventAttendance();
         if(refresher != 0)
         {
          refresher.complete();
         }
         

    });
    }
    
  searchMember(m:any)
  {
    this.initializeMember();
    
    let serVal = m.target.value;
    if(serVal && serVal.trim() != '')
    {
      
      this.showMember = this.showMember.filter((getmember) => {
        return(getmember.EnglishName.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
       
      });
      this.renderer.invokeElementMethod(m.target, 'blur');
      
    }
  }

  searchLocalGuest(lg:any)
  {
    this.initializeLocalGuest();
    
    let serVal = lg.target.value;
    if(serVal && serVal.trim() != '')
    {
      
      this.showLocalGuest = this.showLocalGuest.filter((getlocalguest) => {
        return(getlocalguest.EnglishName.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
       
      });
      this.renderer.invokeElementMethod(lg.target, 'blur');
      
    }
  }

  searchOverseaGuest(og:any)
  {
    this.initializeOverseaGuest();
    
    let serVal = og.target.value;
    if(serVal && serVal.trim() != '')
    {
      
      this.showOverseaGuest = this.showOverseaGuest.filter((getoverseaguest) => {
        return(getoverseaguest.EnglishName.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
       
      });
      this.renderer.invokeElementMethod(og.target, 'blur');
      
    }
  }

  searchNonMember(nm:any)
  {
    this.initializeNonMember();
    
    let serVal = nm.target.value;
    if(serVal && serVal.trim() != '')
    {
      
      this.showNonMember = this.showNonMember.filter((getnonmember) => {
        return(getnonmember.EnglishName.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
       
      });
      this.renderer.invokeElementMethod(nm.target, 'blur');
      
    }
  }


  initializeMember() {
    this.showMember = this.memberitems;
  }

  initializeLocalGuest() {
    this.showLocalGuest = this.localguestitems;
  }

  initializeOverseaGuest() {
    this.showOverseaGuest = this.overseaguestitems;
  }

  
  initializeNonMember() {
    this.showNonMember = this.nonmemberitems;
  }


  showSpecificUser(userid){
    this.ApiService.getUser(userid).subscribe((res:Response) => {
      const getSpecificUser = res.json();
      this.showSpecUser = getSpecificUser;

      this.navCtrl.push(CommitteeDetailPage,{
        data: userid
        
      });
     
      
    });
  /*#endregion */

 
  
  
  
  }

  getEventTitle(evt:any)
  {
    this.initializeEvent();
    
    let serVal = evt.target.value;
    if(serVal && serVal.trim() != '')
    {
      
      this.eventlisting = this.eventlisting.filter((getevent) => {
        return(getevent.EventTitle.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
       
      });
      this.renderer.invokeElementMethod(evt.target, 'blur');
      
    }
  }

  initializeEvent() {
    this.eventlisting = this.items;
  }

  gotoQRDetail(EventID){
    this.ApiService.getSpecificEvent(EventID).subscribe((res:Response) => {
      const getSpecificEvent = res.json();
      this.showSpecificEvent = getSpecificEvent;

      this.navCtrl.push(QrScanPage,{
        data: EventID
      });

    });
    
  }
  
  getEventTitleForAttendance(evt:any)
  {
    this.initializeEventAttendance();
    
    let serVal = evt.target.value;
    if(serVal && serVal.trim() != '')
    {
      
      this.eventlistingattendance = this.eventlistingattendance.filter((getevent) => {
        return(getevent.EventTitle.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
       
      });
      this.renderer.invokeElementMethod(evt.target, 'blur');
      
    }
  }

  initializeEventAttendance() {
    this.eventlistingattendance = this.eventattendanceitems;
  }

  gotoAttendanceDetail(EventID){
    this.ApiService.getSpecificEvent(EventID).subscribe((res:Response) => {
      const getSpecificEvent = res.json();
      this.showSpecificEvent = getSpecificEvent;

      this.navCtrl.push(AttandancelistPage,{
        data: EventID
      });

    });
    
  }

 
  /*#region Scan */
  async showScanAlert() {
    const alert = await this.alertCtrl.create({
      title: 'Alert',
      subTitle: 'Scanning Info',
      message: 'Please select the following programme to scan the attendance.',
      buttons: ['OK']
    });

    await alert.present();
  }

 
    
 

  /*#endregion */


  

  

  

    
}
