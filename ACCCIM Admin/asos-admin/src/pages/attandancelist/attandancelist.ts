import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiserviceProvider } from '../../providers/apiservice/apiservice';
import {Http,Response} from '@angular/http';
import { Renderer } from '@angular/core';
import { CommitteeDetailPage } from '../committee-detail/committee-detail';
/**
 * Generated class for the AttandancelistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-attandancelist',
  templateUrl: 'attandancelist.html',
})
export class AttandancelistPage {
  attendance: string = "present";
  EventID: any;
  presentitems: any;
  getPresentAttendance: any;
  absentitems: any;
  getAbsentAttendance: any;
  showSpecUser: any;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public ApiService: ApiserviceProvider,
              public renderer: Renderer) {

              this.EventID = navParams.get('data');

              this.attendancePresent(0);

  }

  attendancePresent(refresher){

    this.ApiService.PresentAttendance(this.EventID)
      .subscribe((res:Response) => {
        this.presentitems = res.json();
        this.initializePresentAttendance();
         if(refresher != 0)
         {
          refresher.complete();
         }
       
      });

      this.ApiService.AbsentAttendance(this.EventID)
      .subscribe((res:Response) => {
        this.absentitems = res.json();
        this.initializeAbsentAttendance();
         if(refresher != 0)
         {
          refresher.complete();
         }
       
      })
  
    }
    
  getPresentUser(evt:any)
  {
    this.initializePresentAttendance();
    
    let serVal = evt.target.value;
    if(serVal && serVal.trim() != '')
    {
      
      this.getPresentAttendance = this.getPresentAttendance.filter((getuser) => {
        return(getuser.MemberName.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
       
      });
      this.renderer.invokeElementMethod(evt.target, 'blur');
      
    }
  }
  
  initializePresentAttendance() {
    this.getPresentAttendance = this.presentitems;
  }


  getAbsentUser(evt:any)
  {
    this.initializeAbsentAttendance();
    
    let serVal = evt.target.value;
    if(serVal && serVal.trim() != '')
    {
      
      this.getAbsentAttendance = this.getAbsentAttendance.filter((getuser) => {
        return(getuser.MemberName.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
       
      });
      this.renderer.invokeElementMethod(evt.target, 'blur');
      
    }
  }
  
  initializeAbsentAttendance() {
    this.getAbsentAttendance = this.absentitems;
  }
  

  showSpecificUser(userid){
    this.ApiService.getUser(userid).subscribe((res:Response) => {
      const getSpecificUser = res.json();
      this.showSpecUser = getSpecificUser;
  
      this.navCtrl.push(CommitteeDetailPage,{
        data: userid
        
      });
     
      
    });
  
  
  }


  

}
