import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AttandancelistPage } from './attandancelist';

@NgModule({
  declarations: [
    AttandancelistPage,
  ],
  imports: [
    IonicPageModule.forChild(AttandancelistPage),
  ],
})
export class AttandancelistPageModule {}
