import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiserviceProvider } from '../../providers/apiservice/apiservice';
import {Http,Response} from '@angular/http';
import { DomSanitizer } from '@angular/platform-browser';

/**
 * Generated class for the CommitteeDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-committee-detail',
  templateUrl: 'committee-detail.html',
})
export class CommitteeDetailPage {

  UserID : any;
  userCredential: any;
  username: any;
  constructor(public navCtrl: NavController,
             public navParams: NavParams,
             public ApiService: ApiserviceProvider,
             public sanitizer: DomSanitizer) {
    this.UserID = navParams.get('data');
    this.loadSpecificUser();

    
  }

  loadSpecificUser()
  {
    this.ApiService.getUser(this.UserID)
    .subscribe((res:Response) => {
      this.userCredential = res.json();
      //this.username = this.userCredential.EnglishName;
      
  
    });
  }

 
}
