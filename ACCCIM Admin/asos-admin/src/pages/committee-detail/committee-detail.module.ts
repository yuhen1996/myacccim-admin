import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CommitteeDetailPage } from './committee-detail';

@NgModule({
  declarations: [
    CommitteeDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(CommitteeDetailPage),
  ],
})
export class CommitteeDetailPageModule {}
