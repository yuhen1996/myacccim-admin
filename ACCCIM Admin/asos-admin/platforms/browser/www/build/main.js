webpackJsonp([16],{

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccountPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AccountPage = /** @class */ (function () {
    function AccountPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AccountPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AccountPage');
    };
    AccountPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-account',template:/*ion-inline-start:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\account\account.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-buttons left>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n\n    <ion-title>Account</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <img src="https://res.cloudinary.com/cediim8/image/upload/v1523427281/cover-photo.jpg">\n\n  <div text-center class="circle">\n    <p class="circle-text">JD</p>\n  </div>\n\n  <ion-list class="orders-list">\n    <ion-item>\n      <ion-icon item-start name="ios-basket-outline"></ion-icon>\n      <p>My orders</p>\n    </ion-item>\n\n    <ion-item>\n      <ion-icon item-start name="ios-archive-outline"></ion-icon>\n      <p>My returns</p>\n    </ion-item>\n\n    <ion-item>\n      <ion-icon item-start name="md-bicycle"></ion-icon>\n      <p>Premier Delivery</p>\n    </ion-item>\n  </ion-list>\n\n  <ion-list>\n    <ion-item>\n      <ion-icon item-start name="ios-bookmarks-outline"></ion-icon>\n      <p>My details</p>\n    </ion-item>\n\n    <ion-item>\n      <ion-icon item-start name="ios-home-outline"></ion-icon>\n      <p>Address Book</p>\n    </ion-item>\n\n    <ion-item>\n      <ion-icon item-start name="ios-card"></ion-icon>\n      <p>Payment methods</p>\n    </ion-item>\n\n    <ion-item>\n      <ion-icon item-start name="ios-text-outline"></ion-icon>\n      <p>Contact Preferences</p>\n    </ion-item>\n\n    <ion-item>\n      <ion-icon item-start name="ios-contacts-outline"></ion-icon>\n      <p>Social accounts</p>\n    </ion-item>\n  </ion-list>\n\n  <ion-list>\n    <ion-item>\n      <ion-icon item-start name="ios-game-controller-b-outline"></ion-icon>\n      <p>Gift card & vouchers</p>\n    </ion-item>\n  </ion-list>\n\n  <ion-list>\n    <ion-item>\n      <ion-icon item-start name="ios-help-circle-outline"></ion-icon>\n      <p>Need help?</p>\n    </ion-item>\n\n    <ion-item>\n      <ion-icon item-start name="ios-browsers-outline"></ion-icon>\n      <p>Where\'s my order?</p>\n    </ion-item>\n\n    <ion-item>\n      <ion-icon item-start name="ios-browsers-outline"></ion-icon>\n      <p>How do I make a return?</p>\n    </ion-item>\n  </ion-list>\n\n  <ion-item>\n    <ion-icon item-start name="md-log-out"></ion-icon>\n    <p>Sign out</p>\n  </ion-item>\n</ion-content>'/*ion-inline-end:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\account\account.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], AccountPage);
    return AccountPage;
}());

//# sourceMappingURL=account.js.map

/***/ }),

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppSettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppSettingsPage = /** @class */ (function () {
    function AppSettingsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AppSettingsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AppSettingsPage');
    };
    AppSettingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-app-settings',template:/*ion-inline-start:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\app-settings\app-settings.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-buttons left>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n\n    <ion-title>APP SETTINGS</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-list>\n    <ion-list-header>\n      <p>Store setup</p>\n    </ion-list-header>\n\n    <ion-item>\n      <p class="item-title">Country</p>\n      <p>Australia</p>\n    </ion-item>\n\n    <ion-item>\n      <p class="item-title">Change currency</p>\n      <p>$ AUD</p>\n    </ion-item>\n\n    <ion-item>\n      <p class="item-title">Change language</p>\n      <p>Australian English</p>\n    </ion-item>\n\n    <ion-item>\n      <p class="item-title">Change sizes</p>\n      <p>Australian English</p>\n    </ion-item>\n  </ion-list>\n\n  <ion-list>\n    <ion-list-header>\n      <p>Other</p>\n    </ion-list-header>\n\n    <ion-item>\n      <p class="item-title">Clear search history</p>\n    </ion-item>\n\n    <ion-item>\n      <p class="item-title">Help & Contact</p>\n    </ion-item>\n\n    <ion-item>\n      <p class="item-title">Report an issue</p>\n    </ion-item>\n\n    <ion-item>\n      <p class="item-title">Rate the app</p>\n    </ion-item>\n\n    <ion-item>\n      <p class="item-title">Notifications</p>\n    </ion-item>\n\n    <ion-item>\n      <p class="item-title">Clear search history</p>\n    </ion-item>\n\n    <ion-item>\n      <p class="item-title">Vibrate when you add to bag</p>\n      <ion-toggle item-end checked="false"></ion-toggle>\n    </ion-item>\n\n    <ion-item>\n      <p class="item-title">Terms and conditions</p>\n    </ion-item>\n  </ion-list>\n\n  <ion-list>\n    <ion-list-header>\n      <p>Other</p>\n    </ion-list-header>\n\n    <ion-item>\n      <p class="item-title">Build version</p>\n      <p>1.0.0 (100)</p>\n    </ion-item>\n\n    <ion-item>\n      <p class="item-title">Build time</p>\n      <p>2018-01-01 12:00:00</p>\n    </ion-item>\n\n    <ion-item>\n      <p class="item-title">Open-source licenses</p>\n      <p>License details for used open-source software</p>\n    </ion-item>\n  </ion-list>\n'/*ion-inline-end:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\app-settings\app-settings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], AppSettingsPage);
    return AppSettingsPage;
}());

//# sourceMappingURL=app-settings.js.map

/***/ }),

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BagPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(47);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BagPage = /** @class */ (function () {
    function BagPage(navCtrl, navParams, app) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.app = app;
    }
    BagPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BagPage');
    };
    BagPage.prototype.goToSavedItems = function () {
        this.navCtrl.push('SavedItemsPage');
    };
    BagPage.prototype.goToHome = function () {
        this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    BagPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-bag',template:/*ion-inline-start:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\bag\bag.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-buttons left>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n\n    <ion-title>MY BAG</ion-title>\n\n    <ion-buttons right>\n      <button class="navbar-button" ion-button icon-only (click)="goToSavedItems()">\n        <ion-icon name="md-heart-outline"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-col text-center>\n    <img src="https://res.cloudinary.com/cediim8/image/upload/v1523427246/sad-face.png">\n\n    <p class="bag-empty">YOUR BAG IS EMPTY</p>\n\n    <p class="bag-description">Items are reserved for you in your bag for 60 minutes and are then moved to Saved Items.</p>\n\n    <button ion-button full (click)="goToSavedItems()">\n      VIEW SAVED ITEMS\n    </button>\n\n    <button class="start-shopping" ion-button full (click)="goToHome()">\n      START SHOPPING\n    </button>\n  </ion-col>\n</ion-content>'/*ion-inline-end:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\bag\bag.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */]])
    ], BagPage);
    return BagPage;
}());

//# sourceMappingURL=bag.js.map

/***/ }),

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommitteeDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_apiservice_apiservice__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_call_number_ngx__ = __webpack_require__(82);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the CommitteeDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CommitteeDetailPage = /** @class */ (function () {
    function CommitteeDetailPage(navCtrl, navParams, ApiService, sanitizer, callNumber) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.ApiService = ApiService;
        this.sanitizer = sanitizer;
        this.callNumber = callNumber;
        this.UserID = navParams.get('data');
        this.loadSpecificUser();
    }
    CommitteeDetailPage.prototype.loadSpecificUser = function () {
        var _this = this;
        this.ApiService.getUser(this.UserID)
            .subscribe(function (res) {
            _this.userCredential = res.json();
        });
    };
    CommitteeDetailPage.prototype.callJoint = function () {
        this.callNumber.isCallSupported()
            .then(function (response) {
            if (response == true) {
                this.callNumber.callNumber("0175627791", true);
            }
            else {
                // do something else
            }
        });
    };
    CommitteeDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-committee-detail',template:/*ion-inline-start:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\committee-detail\committee-detail.html"*/'<!--\n  Generated template for the CommitteeDetailPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title class="committeetitle">committee-detail</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <div *ngFor="let user of userCredential">\n        <img [src]="sanitizer.bypassSecurityTrustUrl(user.QRCodeImage)">\n\n        <div style="text-align:center">\n          <ion-row>\n            <ion-col col-12>\n                <p style="font-size: 20px;">{{user.EventRegID}}</p>\n                <p style="font-size: 15px;">{{user.UserType}}</p>\n            \n            </ion-col>\n          </ion-row>\n\n         \n                 \n            \n        </div>\n        <ion-list class="orders-list">\n\n        \n          <ion-item>\n            <ion-icon item-start name="md-person"></ion-icon>\n            <p>{{user.EnglishName}}</p>\n          </ion-item>\n\n          <ion-item>\n            <ion-icon item-start name="md-mail"></ion-icon>\n            <p>{{user.Email}}</p>\n          </ion-item>\n          <ion-item>\n            <ion-icon item-start name="md-call"></ion-icon>\n            <p>{{user.Contact}}</p>\n          </ion-item>\n\n          <ion-item>\n            <ion-icon item-start name="md-bookmark"></ion-icon>\n            <p>{{user.Title}}</p>\n          </ion-item>\n        </ion-list>\n\n        <ion-list>\n          <ion-item>\n            <ion-icon item-start name="md-locate"></ion-icon>\n            <p>{{user.Constituent}}</p>\n          </ion-item>\n\n          <ion-item>\n\n\n            <ion-icon item-start name="md-briefcase"></ion-icon>\n            <p>{{user.CompanyName}}</p>\n          </ion-item>\n\n          <ion-item>\n            <ion-icon item-start name="md-people"></ion-icon>\n            <p>{{user.OrganizationName}}</p>\n          </ion-item>\n\n         \n        </ion-list>\n        <button type="button" ion-button (click)="callJoint()">\n\n            <ion-icon item-start name="call"></ion-icon>\n            </button>\n      </div>\n</ion-content>\n'/*ion-inline-end:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\committee-detail\committee-detail.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_apiservice_apiservice__["a" /* ApiserviceProvider */],
            __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["c" /* DomSanitizer */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_call_number_ngx__["a" /* CallNumber */]])
    ], CommitteeDetailPage);
    return CommitteeDetailPage;
}());

//# sourceMappingURL=committee-detail.js.map

/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QrScanPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_apiservice_apiservice__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_barcode_scanner__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_call_number_ngx__ = __webpack_require__(82);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the QrScanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var QrScanPage = /** @class */ (function () {
    function QrScanPage(navCtrl, navParams, ApiService, barcodeScanner, renderer, call, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.ApiService = ApiService;
        this.barcodeScanner = barcodeScanner;
        this.renderer = renderer;
        this.call = call;
        this.alertCtrl = alertCtrl;
        this.scanqrinfo = "scan";
        this.scannedCode = null;
        this.EventID = navParams.get('data');
        this.attendanceRefresh(0);
    }
    QrScanPage.prototype.scanCode = function () {
        var _this = this;
        this.barcodeScanner.scan().then(function (barcodeData) {
            _this.scannedCode = barcodeData.text;
            _this.ApiService.QRScanAttendance(_this.scannedCode, _this.EventID)
                .subscribe(function (result) {
                var alert = _this.alertCtrl.create({
                    title: 'Successfully Scan',
                    subTitle: 'This user has been attend the event.',
                    buttons: ['Ok']
                });
                alert.present();
            }, function (err) {
                //console.log("Failed");
                var alert = _this.alertCtrl.create({
                    title: 'Failed To Scan',
                    subTitle: 'This user has been recorded in attendance list.',
                    buttons: ['Ok']
                });
                alert.present();
            });
        });
    };
    QrScanPage.prototype.attendanceRefresh = function (refresher) {
        var _this = this;
        this.ApiService.getRsvpEventList(this.EventID)
            .subscribe(function (res) {
            _this.items = res.json();
            _this.initializeAttendance();
            if (refresher != 0) {
                refresher.complete();
            }
        });
    };
    QrScanPage.prototype.getUserUpdate = function (usr) {
        this.initializeAttendance();
        var serVal = usr.target.value;
        if (serVal && serVal.trim() != '') {
            this.showRsvpEvent = this.showRsvpEvent.filter(function (getuser) {
                return (getuser.MemberName.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
            });
            this.renderer.invokeElementMethod(usr.target, 'blur');
        }
    };
    QrScanPage.prototype.initializeAttendance = function () {
        this.showRsvpEvent = this.items;
    };
    QrScanPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-qr-scan',template:/*ion-inline-start:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\qr-scan\qr-scan.html"*/'<!--\n  Generated template for the QrScanPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>qr-scan</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n  <ion-segment [(ngModel)]="scanqrinfo">\n      <ion-segment-button value="scan">\n          <p class="item-title" style="color:#ED6D10;">Scan</p>\n    \n        </ion-segment-button>\n    <ion-segment-button value="rsvplist">\n      <p class="item-title" style="color:#ED6D10;">RSVP List</p>\n    </ion-segment-button>\n\n   \n  </ion-segment>\n  <div [ngSwitch]="scanqrinfo">\n    <div *ngSwitchCase="\'rsvplist\'">\n\n      <ion-refresher (ionRefresh)="attendanceRefresh($event);">\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher>\n      <ion-searchbar (ionInput)="getUserUpdate($event);" [debounce]="500" placeholder="Search By English Name Only">\n      </ion-searchbar>\n      <ion-card *ngFor="let rsvpevent of showRsvpEvent">\n        <ion-grid>\n          <ion-row style="margin-top: 5px;">\n\n\n            <ion-col col-8>\n              <h3 style="margin-top: 8px;">{{rsvpevent.MemberRegistrationID}}</h3>\n              <h3 style="margin-top: 8px;">{{rsvpevent.MemberName}}</h3>\n              <h3 style="margin-top: 8px;">{{rsvpevent.MemberType}}</h3>\n             \n            </ion-col>\n            <ion-col col-4>\n              <div *ngIf="rsvpevent.Status">\n                  <ion-icon style="margin-top: 23px; color:green " ios="ios-checkmark-circle" md="md-checkmark-circle"></ion-icon>\n                      \n                      {{rsvpevent.CheckInTime | date: "yyyy/MM/dd"}}\n              </div>\n               \n                   \n                  \n             \n              \n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-card>\n\n    </div>\n  </div>\n\n  <div [ngSwitch]="scanqrinfo">\n    <div *ngSwitchCase="\'scan\'">\n      <div style="text-align:center">\n        <ion-row>\n          <ion-col col-12>\n              <button style="width:100%;" ion-button (click)="scanCode()">Scan</button>\n          </ion-col>\n        </ion-row>\n         \n      </div>\n      \n    </div>\n  </div>\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\qr-scan\qr-scan.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_apiservice_apiservice__["a" /* ApiserviceProvider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["V" /* Renderer */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_call_number_ngx__["a" /* CallNumber */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], QrScanPage);
    return QrScanPage;
}());

//# sourceMappingURL=qr-scan.js.map

/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HelpFaqPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HelpFaqPage = /** @class */ (function () {
    function HelpFaqPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    HelpFaqPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HelpFaqPage');
    };
    HelpFaqPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-help-faq',template:/*ion-inline-start:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\help-faq\help-faq.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-buttons left>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n\n    <ion-title>Help & FAQs</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n</ion-content>'/*ion-inline-end:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\help-faq\help-faq.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], HelpFaqPage);
    return HelpFaqPage;
}());

//# sourceMappingURL=help-faq.js.map

/***/ }),

/***/ 112:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductColourPopOverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProductColourPopOverPage = /** @class */ (function () {
    function ProductColourPopOverPage(navCtrl, navParams, viewController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewController = viewController;
    }
    ProductColourPopOverPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProductColourPopOverPage');
    };
    ProductColourPopOverPage.prototype.closePopover = function () {
        this.viewController.dismiss();
    };
    ProductColourPopOverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-product-colour-pop-over',template:/*ion-inline-start:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\product-colour-pop-over\product-colour-pop-over.html"*/'<ion-list no-margin no-lines>\n  <ion-item>\n    <button ion-item no-lines (click)="closePopover()">\n      BLACK\n    </button>\n  </ion-item>\n\n  <ion-item>\n    <button ion-item no-lines (click)="closePopover()">\n      BLUE\n    </button>\n  </ion-item>\n\n  <ion-item>\n    <button ion-item no-lines (click)="closePopover()">\n      RED\n    </button>\n  </ion-item>\n\n  <ion-item>\n    <button ion-item no-lines (click)="closePopover()">\n      GREEN\n    </button>\n  </ion-item>\n\n  <ion-item>\n    <button ion-item no-lines (click)="closePopover()">\n      YELLOW\n    </button>\n  </ion-item>\n</ion-list>'/*ion-inline-end:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\product-colour-pop-over\product-colour-pop-over.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */]])
    ], ProductColourPopOverPage);
    return ProductColourPopOverPage;
}());

//# sourceMappingURL=product-colour-pop-over.js.map

/***/ }),

/***/ 123:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 123;

/***/ }),

/***/ 165:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/account/account.module": [
		298,
		15
	],
	"../pages/app-settings/app-settings.module": [
		299,
		14
	],
	"../pages/bag/bag.module": [
		300,
		13
	],
	"../pages/category/category.module": [
		313,
		3
	],
	"../pages/committee-detail/committee-detail.module": [
		305,
		12
	],
	"../pages/filter-multi-selection/filter-multi-selection.module": [
		301,
		2
	],
	"../pages/filter-range/filter-range.module": [
		302,
		1
	],
	"../pages/filter/filter.module": [
		303,
		11
	],
	"../pages/help-faq/help-faq.module": [
		306,
		10
	],
	"../pages/men-women-category-pop-over/men-women-category-pop-over.module": [
		304,
		9
	],
	"../pages/product-colour-pop-over/product-colour-pop-over.module": [
		307,
		8
	],
	"../pages/product-details/product-details.module": [
		308,
		7
	],
	"../pages/product-size-pop-over/product-size-pop-over.module": [
		309,
		6
	],
	"../pages/products/products.module": [
		310,
		0
	],
	"../pages/qr-scan/qr-scan.module": [
		312,
		5
	],
	"../pages/recommended-pop-over/recommended-pop-over.module": [
		311,
		4
	],
	"../pages/saved-items/saved-items.module": [
		170
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 165;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 170:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SavedItemsPageModule", function() { return SavedItemsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__saved_items__ = __webpack_require__(84);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SavedItemsPageModule = /** @class */ (function () {
    function SavedItemsPageModule() {
    }
    SavedItemsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__saved_items__["a" /* SavedItemsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__saved_items__["a" /* SavedItemsPage */]),
            ],
        })
    ], SavedItemsPageModule);
    return SavedItemsPageModule;
}());

//# sourceMappingURL=saved-items.module.js.map

/***/ }),

/***/ 212:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_apiservice_apiservice__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(213);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LoginPage = /** @class */ (function () {
    function LoginPage(nav, forgotCtrl, menu, toastCtrl, formBuilder, ApiService, alertCtrl, loadingCtrl, storage, view) {
        this.nav = nav;
        this.forgotCtrl = forgotCtrl;
        this.menu = menu;
        this.toastCtrl = toastCtrl;
        this.formBuilder = formBuilder;
        this.ApiService = ApiService;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.view = view;
        this.menu.swipeEnable(false);
        this.loginForm = this.formBuilder.group({
            'user': ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required])],
            'pass': ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required])]
        });
    }
    LoginPage.prototype.login = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: '',
            duration: 5000
        });
        loading.present();
        this.ApiService.adminLogin(this.user, this.pass).subscribe(function (res) {
            var checkLogin = res.json();
            _this.result = checkLogin;
            var alert = _this.alertCtrl.create({
                title: 'Successfully Signed In',
                subTitle: 'Welcome To ACCCIM Administrative!',
                buttons: ['Ok']
            });
            alert.present();
            _this.storage.set("userid", _this.result);
            _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
        }, function (err) {
            //console.log("Failed");
            var alert = _this.alertCtrl.create({
                title: 'Sign In Failed',
                subTitle: 'ID or password is incorrect',
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    LoginPage.prototype.forgotPass = function () {
        var _this = this;
        var forgot = this.forgotCtrl.create({
            title: 'Forgot Password?',
            message: "Enter you email address to send a reset link password.",
            inputs: [
                {
                    name: 'username',
                    placeholder: 'username',
                    type: 'username'
                },
                {
                    name: 'email',
                    placeholder: 'Email',
                    type: 'email'
                },
                {
                    name: 'new password',
                    placeholder: 'new password',
                    type: 'password'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Send',
                    handler: function (data) {
                        console.log('Send clicked');
                        var toast = _this.toastCtrl.create({
                            message: 'Email was sended successfully',
                            duration: 3000,
                            position: 'top',
                            cssClass: 'dark-trans',
                            closeButtonText: 'OK',
                            showCloseButton: true
                        });
                        toast.present();
                    }
                }
            ]
        });
        forgot.present();
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\login\login.html"*/'<ion-header>\n  <ion-navbar>\n      <ion-title>Sign In</ion-title>\n\n  </ion-navbar>\n</ion-header>\n<!--\nGenerated template for the LoginPage page.\n\nSee http://ionicframework.com/docs/components/#navigation for more info on\nIonic pages and navigation.\n-->\n<!-- -->\n<ion-content padding class="animated fadeIn login auth-page">\n<div class="login-content">\n\n  <!-- Logo -->\n  <div padding-horizontal text-center class="animated fadeInDown">\n    <div class="logo">\n      <ion-img style="width: 160px; height: 80px;background:none;" src="../../assets/imgs/ACCIMlogo.png"></ion-img>\n    </div>\n    <h2 ion-text class="text-primary">\n      <strong>ACCCIM Administrative</strong>\n    </h2>\n  </div>\n\n  <!-- Login form -->\n<div [formGroup]="loginForm"> \n  <div class="list-form">\n    <ion-item>\n      <ion-label floating>\n        <ion-icon name="person" item-start class="text-primary"></ion-icon>\n        ID \n      </ion-label>\n      <ion-input type="text" [(ngModel)]="user" name="user" formControlName="user" id="user"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label floating>\n        <ion-icon name="lock" item-start class="text-primary"></ion-icon>\n        Password\n      </ion-label>\n      <ion-input type="password" [(ngModel)]="pass" name="pass" formControlName="pass" id="pass"></ion-input>\n    </ion-item>\n  </div>\n</div>\n<!--\n  <p text-right ion-text color="secondary" tappable (click)="forgotPass()"><strong>Forgot Password?</strong></p>\n-->\n  <div>\n\n  <br>\n    <button ion-button icon-start block color="dark" tappable [disabled]="!loginForm.valid" (click)="login()">\n      <ion-icon name="log-in"></ion-icon>\n      SIGN IN\n    </button>\n  \n   \n  </div>\n\n\n \n</div>\n</ion-content>\n\n'/*ion-inline-end:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\login\login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_4__providers_apiservice_apiservice__["a" /* ApiserviceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 214:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FilterPage = /** @class */ (function () {
    function FilterPage(navCtrl, navParams, viewController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewController = viewController;
    }
    FilterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FilterPage');
    };
    FilterPage.prototype.closeFilter = function () {
        this.viewController.dismiss();
    };
    FilterPage.prototype.goToMultiSelection = function (selectionType) {
        this.navCtrl.push('FilterMultiSelectionPage', { selectionType: selectionType });
    };
    FilterPage.prototype.goToRangeSelection = function (selectionType) {
        this.navCtrl.push('FilterRangePage', { selectionType: selectionType });
    };
    FilterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-filter',template:/*ion-inline-start:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\filter\filter.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>FILTER</ion-title>\n\n    <ion-buttons right>\n      <button class="navbar-button" ion-button icon-only (click)="closeFilter()">\n        <ion-icon name="md-close-circle"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-item (click)="goToMultiSelection(\'Product Type\')">\n    <p class="filter-type" item-start>Product Type</p>\n    <p item-end>All</p>\n  </ion-item>\n\n  <ion-item (click)="goToMultiSelection(\'Range\')">\n    <p class="filter-type" item-start>Range</p>\n    <p item-end>All</p>\n  </ion-item>\n\n  <ion-item (click)="goToMultiSelection(\'Brand\')">\n    <p class="filter-type" item-start>Brand</p>\n    <p item-end>All</p>\n  </ion-item>\n\n  <ion-item (click)="goToMultiSelection(\'Multipacks\')">\n    <p class="filter-type" item-start>Multipacks</p>\n    <p item-end>All</p>\n  </ion-item>\n\n  <ion-item (click)="goToMultiSelection(\'Size\')">\n    <p class="filter-type" item-start>Size</p>\n    <p item-end>All</p>\n  </ion-item>\n\n  <ion-item (click)="goToRangeSelection(\'Price Range\')">\n    <p class="filter-type" item-start>Price Range</p>\n    <p item-end>$20 - $500</p>\n  </ion-item>\n\n  <ion-item (click)="goToMultiSelection(\'Colour\')">\n    <p class="filter-type" item-start>Colour</p>\n    <p item-end>All</p>\n  </ion-item>\n</ion-content>\n\n<ion-footer padding>\n  <button ion-button full (click)="closeFilter()">\n    <p>DONE</p>\n  </button>\n</ion-footer>'/*ion-inline-end:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\filter\filter.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */]])
    ], FilterPage);
    return FilterPage;
}());

//# sourceMappingURL=filter.js.map

/***/ }),

/***/ 215:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RecommendedPopOverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RecommendedPopOverPage = /** @class */ (function () {
    function RecommendedPopOverPage(navCtrl, navParams, viewController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewController = viewController;
    }
    RecommendedPopOverPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RecommendedPopOverPage');
    };
    RecommendedPopOverPage.prototype.selectAndClosePopover = function () {
        this.viewController.dismiss();
    };
    RecommendedPopOverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-recommended-pop-over',template:/*ion-inline-start:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\recommended-pop-over\recommended-pop-over.html"*/'<ion-list no-margin no-lines>\n  <ion-item>\n    <button ion-item no-lines (click)="selectAndClosePopover()">\n      Recommended\n    </button>\n  </ion-item>\n\n  <ion-item>\n    <button ion-item no-lines (click)="selectAndClosePopover()">\n      What\'s New\n    </button>\n  </ion-item>\n\n  <ion-item>\n    <button ion-item no-lines (click)="selectAndClosePopover()">\n      Price - Low to High\n    </button>\n  </ion-item>\n\n  <ion-item>\n    <button ion-item no-lines (click)="selectAndClosePopover()">\n      Price - High to Low\n    </button>\n  </ion-item>\n</ion-list>'/*ion-inline-end:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\recommended-pop-over\recommended-pop-over.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */]])
    ], RecommendedPopOverPage);
    return RecommendedPopOverPage;
}());

//# sourceMappingURL=recommended-pop-over.js.map

/***/ }),

/***/ 216:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenWomenCategoryPopOverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MenWomenCategoryPopOverPage = /** @class */ (function () {
    function MenWomenCategoryPopOverPage(navCtrl, navParams, viewController, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewController = viewController;
        this.events = events;
    }
    MenWomenCategoryPopOverPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MenWomenCategoryPopOverPage');
    };
    MenWomenCategoryPopOverPage.prototype.selectAndClosePopover = function (selection) {
        this.events.publish('genderShoppingChanged', selection == 'Men');
        this.viewController.dismiss();
    };
    MenWomenCategoryPopOverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-men-women-category-pop-over',template:/*ion-inline-start:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\men-women-category-pop-over\men-women-category-pop-over.html"*/'<ion-list no-margin no-lines>\n  <ion-item>\n    <button ion-item no-lines (click)="selectAndClosePopover(\'Men\')">\n      Men\n    </button>\n  </ion-item>\n\n  <ion-item>\n    <button ion-item no-lines (click)="selectAndClosePopover(\'Women\')">\n      Women\n    </button>\n  </ion-item>\n</ion-list>'/*ion-inline-end:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\men-women-category-pop-over\men-women-category-pop-over.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
    ], MenWomenCategoryPopOverPage);
    return MenWomenCategoryPopOverPage;
}());

//# sourceMappingURL=men-women-category-pop-over.js.map

/***/ }),

/***/ 217:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductDetailsPage; });
/* unused harmony export Product */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__product_size_pop_over_product_size_pop_over__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProductDetailsPage = /** @class */ (function () {
    function ProductDetailsPage(navCtrl, navParams, popoverCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.description = '';
        this.price = 0;
        this.photos = [];
        this.otherProducts = [];
        this.isMenSelected = navParams.get('isMenSelected');
        if (this.isMenSelected) {
            this.initialiseManProductDetails();
        }
        else {
            this.initialiseWomanProductDetails();
        }
    }
    ProductDetailsPage.prototype.initialiseManProductDetails = function () {
        this.description = 'Lee Sherpa Rider Denim Jacket Mid Wash';
        this.price = 218;
        this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523345058/product-details-1.jpg');
        this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523345058/product-details-2.jpg');
        this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523345058/product-details-3.jpg');
        this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523345058/product-details-4.jpg');
        this.otherProducts.push(new Product('Pull&Bear Denim Jacket In Black', 'https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-1.jpg', 40, 30));
        this.otherProducts.push(new Product('Liquor N Poker Oversized Denim Jacket Stonewas', 'https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-2.jpg', 89, 79));
        this.otherProducts.push(new Product('Puma T7 Vintage Track Jacket In White 57498506', 'https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-3.jpg', 110, 99));
        this.otherProducts.push(new Product('New Look Cotton Bomber Jacket In Burgundy', 'https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-8.jpg', 50, 35));
    };
    ProductDetailsPage.prototype.initialiseWomanProductDetails = function () {
        this.description = 'ASOS Cotton Mini Shirt Dress';
        this.price = 40;
        this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523417552/product-details-women-1.jpg');
        this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523417552/product-details-2-women.jpg');
        this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523417552/product-details-3-women.jpg');
        this.photos.push('https://res.cloudinary.com/cediim8/image/upload/v1523417552/product-details-4-women.jpg');
        this.otherProducts.push(new Product('Stradivarius Polka Dot Shirt Dress', 'https://res.cloudinary.com/cediim8/image/upload/v1523415018/women-product-2', 40, 30));
        this.otherProducts.push(new Product('ASOS Ultimate Rolled Sleeve T-Shirt Dress With Tab', 'https://res.cloudinary.com/cediim8/image/upload/v1523415018/women-product-3', 30, 25));
        this.otherProducts.push(new Product('Boohoo One Shoulder Floral Midi Dress', 'https://res.cloudinary.com/cediim8/image/upload/v1523415018/women-product-4', 40, 30));
        this.otherProducts.push(new Product('Boohoo Off Shoulder Lemon Print Dress', 'https://res.cloudinary.com/cediim8/image/upload/v1523415018/women-product-5', 44, 40));
    };
    ProductDetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProductDetailsPage');
    };
    ProductDetailsPage.prototype.sizePopOver = function (myEvent) {
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_2__product_size_pop_over_product_size_pop_over__["a" /* ProductSizePopOverPage */]);
        popover.present({
            ev: myEvent
        });
    };
    ProductDetailsPage.prototype.likeUnlike = function () {
        this.isLiked = !this.isLiked;
    };
    ProductDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-product-details',template:/*ion-inline-start:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\product-details\product-details.html"*/'<ion-header no-border>\n  <ion-navbar transparent>\n    <ion-buttons right>\n      <button class="navbar-button" ion-button icon-only>\n        <ion-icon name="md-share"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content fullscreen padding>\n  <ion-slides pager>\n    <ion-slide>\n      <img src="{{ photos[0] }}">\n    </ion-slide>\n\n    <ion-slide>\n      <img src="{{ photos[1] }}">\n    </ion-slide>\n\n    <ion-slide>\n      <img src="{{ photos[2] }}">\n    </ion-slide>\n\n    <ion-slide>\n      <img src="{{ photos[3] }}">\n    </ion-slide>\n  </ion-slides>\n\n  <ion-grid id="product-details">\n    <ion-row>\n      <ion-col text-center>\n        <button class="play-video" ion-button clear icon-only>\n          <ion-icon name="ios-play-outline"></ion-icon>\n          <p>VIDEO</p>\n        </button>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-item>\n        <p class="product-description">{{ description }}</p>\n        <p class="product-price">${{ price }}.00</p>\n      </ion-item>\n    </ion-row>\n\n    <ion-row>\n      <ion-col>\n        <p class="product-count">100</p>\n      </ion-col>\n\n      <ion-col>\n        <ion-item class="product-size" (click)="sizePopOver($event)">\n          <p item-start>SIZE</p>\n          <ion-icon item-end name="ios-arrow-down"></ion-icon>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col col-10>\n        <button class="add-to-bag" ion-button full icon-only>\n          <p>ADD TO BAG</p>\n        </button>\n      </ion-col>\n\n      <ion-col col-2>\n        <button class="product-like" item-end ion-button clear icon-only (click)="likeUnlike()">\n          <ion-icon *ngIf="isLiked" name="ios-heart"></ion-icon>\n          <ion-icon *ngIf="!isLiked" name="ios-heart-outline"></ion-icon>\n        </button>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <p class="product-external-links">FREE SHIPPING AND RETURNS</p>\n    </ion-row>\n\n    <ion-row>\n      <p class="product-external-links">PRODUCT DETAILS</p>\n    </ion-row>\n\n    <ion-row>\n      <p class="product-external-links">SIZE GUIDE</p>\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid id="related-products">\n    <ion-item class="buy-look-row">\n      <p class="buy-the-look" item-start>BUY THE LOOK</p>\n      <p item-end>{{ otherProducts.length }} items</p>\n    </ion-item>\n\n    <ion-scroll scrollX="true" scroll-avatar>\n      <ion-col class="scroll-item" padding>\n        <img class="product-image" src="{{ otherProducts[0].picture }}">\n\n        <ion-item no-lines>\n          <p class="discount-price" item-start>${{ otherProducts[0].discountPrice }}</p>\n\n          <p class="product-price">${{ otherProducts[0].price }}</p>\n\n          <button class="product-like" item-end ion-button clear icon-only>\n            <ion-icon name="md-heart-outline"></ion-icon>\n          </button>\n        </ion-item>\n\n        <p class="product-brand-description">{{ otherProducts[0].name }}</p>\n      </ion-col>\n\n      <ion-col class="scroll-item" padding>\n        <img class="product-image" src="{{ otherProducts[1].picture }}">\n\n        <ion-item no-lines>\n          <p class="discount-price" item-start>${{ otherProducts[1].discountPrice }}</p>\n\n          <p class="product-price">${{ otherProducts[1].price }}</p>\n\n          <button class="product-like" item-end ion-button clear icon-only>\n            <ion-icon name="md-heart-outline"></ion-icon>\n          </button>\n        </ion-item>\n\n        <p class="product-brand-description">{{ otherProducts[1].name }}</p>\n      </ion-col>\n\n      <ion-col class="scroll-item" padding>\n        <img class="product-image" src="{{ otherProducts[2].picture }}">\n\n        <ion-item no-lines>\n          <p class="discount-price" item-start>${{ otherProducts[2].discountPrice }}</p>\n\n          <p class="product-price">${{ otherProducts[2].price }}</p>\n\n          <button class="product-like" item-end ion-button clear icon-only>\n            <ion-icon name="md-heart-outline"></ion-icon>\n          </button>\n        </ion-item>\n\n        <p class="product-brand-description">{{ otherProducts[2].name }}</p>\n      </ion-col>\n\n      <ion-col class="scroll-item" padding>\n        <img class="product-image" src="{{ otherProducts[3].picture }}">\n\n        <ion-item no-lines>\n          <p class="discount-price" item-start>${{ otherProducts[3].discountPrice }}</p>\n\n          <p class="product-price">${{ otherProducts[3].price }}</p>\n\n          <button class="product-like" item-end ion-button clear icon-only>\n            <ion-icon name="md-heart-outline"></ion-icon>\n          </button>\n        </ion-item>\n\n        <p class="product-brand-description">{{ otherProducts[3].name }}</p>\n      </ion-col>\n    </ion-scroll>\n  </ion-grid>\n</ion-content>'/*ion-inline-end:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\product-details\product-details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* PopoverController */]])
    ], ProductDetailsPage);
    return ProductDetailsPage;
}());

var Product = /** @class */ (function () {
    function Product(name, picture, price, discountPrice) {
        this.name = name;
        this.picture = picture;
        this.price = price;
        this.discountPrice = discountPrice;
    }
    return Product;
}());

//# sourceMappingURL=product-details.js.map

/***/ }),

/***/ 218:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(239);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 239:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(294);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_men_women_category_pop_over_men_women_category_pop_over__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_bag_bag__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_saved_items_saved_items__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_account_account__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_app_settings_app_settings__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_help_faq_help_faq__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_saved_items_saved_items_module__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_recommended_pop_over_recommended_pop_over__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_filter_filter__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_product_size_pop_over_product_size_pop_over__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_product_colour_pop_over_product_colour_pop_over__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_login_login__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__providers_apiservice_apiservice__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__angular_common_http__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__angular_http__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_storage__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_product_details_product_details__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_committee_detail_committee_detail__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_qr_scan_qr_scan__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ionic_native_barcode_scanner__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__ionic_native_call_number_ngx__ = __webpack_require__(82);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_men_women_category_pop_over_men_women_category_pop_over__["a" /* MenWomenCategoryPopOverPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_recommended_pop_over_recommended_pop_over__["a" /* RecommendedPopOverPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_product_size_pop_over_product_size_pop_over__["a" /* ProductSizePopOverPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_product_colour_pop_over_product_colour_pop_over__["a" /* ProductColourPopOverPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_bag_bag__["a" /* BagPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_account_account__["a" /* AccountPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_app_settings_app_settings__["a" /* AppSettingsPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_help_faq_help_faq__["a" /* HelpFaqPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_filter_filter__["a" /* FilterPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_product_details_product_details__["a" /* ProductDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_committee_detail_committee_detail__["a" /* CommitteeDetailPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_qr_scan_qr_scan__["a" /* QrScanPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/account/account.module#AccountPageModule', name: 'AccountPage', segment: 'account', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/app-settings/app-settings.module#AppSettingsPageModule', name: 'AppSettingsPage', segment: 'app-settings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/bag/bag.module#BagPageModule', name: 'BagPage', segment: 'bag', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/filter-multi-selection/filter-multi-selection.module#FilterMultiSelectionPageModule', name: 'FilterMultiSelectionPage', segment: 'filter-multi-selection', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/filter-range/filter-range.module#FilterRangePageModule', name: 'FilterRangePage', segment: 'filter-range', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/filter/filter.module#FilterPageModule', name: 'FilterPage', segment: 'filter', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/men-women-category-pop-over/men-women-category-pop-over.module#MenWomenCategoryPopOverPageModule', name: 'MenWomenCategoryPopOverPage', segment: 'men-women-category-pop-over', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/committee-detail/committee-detail.module#CommitteeDetailPageModule', name: 'CommitteeDetailPage', segment: 'committee-detail', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/help-faq/help-faq.module#HelpFaqPageModule', name: 'HelpFaqPage', segment: 'help-faq', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/product-colour-pop-over/product-colour-pop-over.module#ProductColourPopOverPageModule', name: 'ProductColourPopOverPage', segment: 'product-colour-pop-over', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/product-details/product-details.module#ProductDetailsPageModule', name: 'ProductDetailsPage', segment: 'product-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/product-size-pop-over/product-size-pop-over.module#ProductSizePopOverPageModule', name: 'ProductSizePopOverPage', segment: 'product-size-pop-over', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/products/products.module#ProductsPageModule', name: 'ProductsPage', segment: 'products', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/recommended-pop-over/recommended-pop-over.module#RecommendedPopOverPageModule', name: 'RecommendedPopOverPage', segment: 'recommended-pop-over', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/saved-items/saved-items.module#SavedItemsPageModule', name: 'SavedItemsPage', segment: 'saved-items', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/qr-scan/qr-scan.module#QrScanPageModule', name: 'QrScanPage', segment: 'qr-scan', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/category/category.module#CategoryPageModule', name: 'CategoryPage', segment: 'category', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_13__pages_saved_items_saved_items_module__["SavedItemsPageModule"],
                __WEBPACK_IMPORTED_MODULE_22__ionic_storage__["a" /* IonicStorageModule */].forRoot({ name: '__mydb',
                    driverOrder: ['indexeddb', 'sqlite', 'websql'] }),
                __WEBPACK_IMPORTED_MODULE_21__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_20__angular_common_http__["b" /* HttpClientModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_men_women_category_pop_over_men_women_category_pop_over__["a" /* MenWomenCategoryPopOverPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_recommended_pop_over_recommended_pop_over__["a" /* RecommendedPopOverPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_product_size_pop_over_product_size_pop_over__["a" /* ProductSizePopOverPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_product_colour_pop_over_product_colour_pop_over__["a" /* ProductColourPopOverPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_bag_bag__["a" /* BagPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_saved_items_saved_items__["a" /* SavedItemsPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_account_account__["a" /* AccountPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_app_settings_app_settings__["a" /* AppSettingsPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_help_faq_help_faq__["a" /* HelpFaqPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_filter_filter__["a" /* FilterPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_product_details_product_details__["a" /* ProductDetailsPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_committee_detail_committee_detail__["a" /* CommitteeDetailPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_qr_scan_qr_scan__["a" /* QrScanPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_19__providers_apiservice_apiservice__["a" /* ApiserviceProvider */],
                __WEBPACK_IMPORTED_MODULE_26__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
                __WEBPACK_IMPORTED_MODULE_27__ionic_native_call_number_ngx__["a" /* CallNumber */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 294:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_bag_bag__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_saved_items_saved_items__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_account_account__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_app_settings_app_settings__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_help_faq_help_faq__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_login_login__ = __webpack_require__(212);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_10__pages_login_login__["a" /* LoginPage */];
        this.initializeApp();
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.goToHome = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */]);
    };
    MyApp.prototype.goToBag = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_bag_bag__["a" /* BagPage */]);
    };
    MyApp.prototype.goToSavedItems = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_6__pages_saved_items_saved_items__["a" /* SavedItemsPage */]);
    };
    MyApp.prototype.goToMyAccount = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_account_account__["a" /* AccountPage */]);
    };
    MyApp.prototype.goToAppSettings = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_8__pages_app_settings_app_settings__["a" /* AppSettingsPage */]);
    };
    MyApp.prototype.goToHelpAndFaq = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_9__pages_help_faq_help_faq__["a" /* HelpFaqPage */]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\app\app.html"*/'<ion-menu [content]="content">\n  <ion-header>\n    <ion-toolbar>\n      <ion-title class="welcome-title">\n        <p>Hey,\n          <strong>John Doe</strong>\n        </p>\n      </ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <ion-list class="menu-list" no-lines>\n      <ion-item class="side-menu-item" (click)="goToHome()">\n        <ion-icon name="ios-home-outline" item-start></ion-icon>\n        <button menuClose ion-item>HOME</button>\n      </ion-item>\n\n      <ion-item class="side-menu-item" (click)="goToBag()">\n        <ion-icon name="ios-basket-outline" item-start></ion-icon>\n        <button menuClose ion-item>BAG</button>\n      </ion-item>\n\n      <ion-item class="side-menu-item" (click)="goToSavedItems()">\n        <ion-icon name="md-heart-outline" item-start></ion-icon>\n        <button menuClose ion-item>SAVED ITEMS</button>\n      </ion-item>\n\n      <ion-item class="side-menu-item" (click)="goToMyAccount()">\n        <ion-icon name="ios-person-outline" item-start></ion-icon>\n        <button menuClose ion-item>MY ACCOUNT</button>\n      </ion-item>\n\n      <ion-item class="side-menu-item" (click)="goToAppSettings()">\n        <ion-icon name="ios-settings-outline" item-start></ion-icon>\n        <button menuClose ion-item>APP SETTINGS</button>\n      </ion-item>\n\n      <ion-item class="side-menu-item" (click)="goToHelpAndFaq()">\n        <ion-icon name="ios-information-circle-outline" item-start></ion-icon>\n        <button menuClose ion-item>HELP & FAQS</button>\n      </ion-item>\n    </ion-list>\n\n    <ion-list class="menu-list">\n      <ion-list-header>\n        <p>MORE ASOS</p>\n      </ion-list-header>\n\n      <ion-item>\n        <p>Unlimited Express Shipping</p>\n      </ion-item>\n\n      <ion-item>\n        <p>10% Student Discount</p>\n      </ion-item>\n    </ion-list>\n\n    <ion-list class="menu-list">\n      <ion-list-header>\n        <p>TELL US WHAT YOU THINK</p>\n      </ion-list-header>\n\n      <ion-item>\n        <p>Help Improve the App</p>\n      </ion-item>\n\n      <ion-item>\n        <p>Rate the App</p>\n      </ion-item>\n    </ion-list>\n\n    <ion-list class="menu-list">\n      <ion-item class="side-menu-item">\n        <ion-icon name="md-log-out" item-start></ion-icon>\n        <button menuClose ion-item>SIGN OUT</button>\n      </ion-item>\n    </ion-list>\n\n    <ion-item>\n      <p>App Version 1.0.0 (100)</p>\n    </ion-item>\n  </ion-content>\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 41:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiserviceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(167);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the ApiserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var httpOptions = {
    headers: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json' })
};
var ApiserviceProvider = /** @class */ (function () {
    function ApiserviceProvider(http, https) {
        this.http = http;
        this.https = https;
        this.AdminLoginAPI = "https://api.acccim-registration.org.my/api/AdminSignIn?id=";
        this.ShowAllMember = "https://api.acccim-registration.org.my/api/DisplayOnlyMember";
        this.ShowAllNonMember = "https://api.acccim-registration.org.my/api/DisplayOnlyNonMember";
        this.ShowAllLocalGuest = "http://api.acccim-registration.org.my/api/DisplayOnlyLocalGuest";
        this.ShowAllOverseaGuest = "http://api.acccim-registration.org.my/api/DisplayOnlyOverseaGuest";
        this.getUserData = "https://api.acccim-registration.org.my/api/UserRegistrationData?id=";
        this.TotalMember = "https://api.acccim-registration.org.my/api/TotalMember";
        this.TotalNonMember = "https://api.acccim-registration.org.my/api/TotalNonMember";
        this.TotalLocalGuest = "https://api.acccim-registration.org.my/api/TotalLocalGuest";
        this.TotalOverseaGuest = "https://api.acccim-registration.org.my/api/TotalOverseaGuest";
        this.TotalDiamondSponsor = "https://api.acccim-registration.org.my/api/TotalDiamondSponsors";
        this.TotalPlatinumSponsor = "https://api.acccim-registration.org.my/api/TotalPlatinumSponsors";
        this.TotalGoldSponsor = "https://api.acccim-registration.org.my/api/TotalGoldSponsors";
        this.TotalSilverSponsor = "https://api.acccim-registration.org.my/api/TotalSilverSponsors";
        this.TotalSupportingSponsor = "https://api.acccim-registration.org.my/api/TotalSupportingSponsors";
        this.TotalEvent = "https://api.acccim-registration.org.my/api/TotalEvent";
        this.eventListing = "https://api.acccim-registration.org.my/api/EventListing";
        this.specificevent = "https://api.acccim-registration.org.my/api/SpecificEvent?eventid=";
        this.rsvpeventlist = "https://api.acccim-registration.org.my/api/RsvpEventList?Id=";
        this.scanattendance = "https://api.acccim-registration.org.my/api/Attendance?RegistrationID=";
    }
    //Scan Attendance 
    ApiserviceProvider.prototype.QRScanAttendance = function (userid, eventid) {
        var uid = userid;
        var eid = eventid;
        return this.https.put(this.scanattendance + uid + '&&EventID=' + eid, httpOptions).pipe();
    };
    //User Login
    ApiserviceProvider.prototype.adminLogin = function (user, pass) {
        var id = user;
        var pw = pass;
        if (id != null && pw != null) {
            return this.https.get(this.AdminLoginAPI + id + '&&pw=' + pw);
        }
    };
    ApiserviceProvider.prototype.getAllMember = function () {
        return this.https.get(this.ShowAllMember);
    };
    ApiserviceProvider.prototype.getAllNonMember = function () {
        return this.https.get(this.ShowAllNonMember);
    };
    ApiserviceProvider.prototype.getAllLocalGuest = function () {
        return this.https.get(this.ShowAllLocalGuest);
    };
    ApiserviceProvider.prototype.getAllOverseaGuest = function () {
        return this.https.get(this.ShowAllOverseaGuest);
    };
    ApiserviceProvider.prototype.getUser = function (id) {
        var userid = id;
        return this.https.get(this.getUserData + userid);
    };
    /*Admin Dashboard*/
    ApiserviceProvider.prototype.getTotalMember = function () {
        return this.https.get(this.TotalMember);
    };
    ApiserviceProvider.prototype.getTotalNonMember = function () {
        return this.https.get(this.TotalNonMember);
    };
    ApiserviceProvider.prototype.getTotalEvent = function () {
        return this.https.get(this.TotalEvent);
    };
    ApiserviceProvider.prototype.getLocalGuest = function () {
        return this.https.get(this.TotalLocalGuest);
    };
    ApiserviceProvider.prototype.getOverseaGuest = function () {
        return this.https.get(this.TotalOverseaGuest);
    };
    ApiserviceProvider.prototype.getDiamondSponsor = function () {
        return this.https.get(this.TotalDiamondSponsor);
    };
    ApiserviceProvider.prototype.getPlatinumSponsor = function () {
        return this.https.get(this.TotalPlatinumSponsor);
    };
    ApiserviceProvider.prototype.getGoldSponsor = function () {
        return this.https.get(this.TotalGoldSponsor);
    };
    ApiserviceProvider.prototype.getSilverSponsor = function () {
        return this.https.get(this.TotalSilverSponsor);
    };
    ApiserviceProvider.prototype.getSupportingSponsor = function () {
        return this.https.get(this.TotalSupportingSponsor);
    };
    ApiserviceProvider.prototype.getEvent = function () {
        return this.https.get(this.eventListing);
    };
    ApiserviceProvider.prototype.getSpecificEvent = function (evtid) {
        var eventid = evtid;
        return this.https.get(this.specificevent + eventid);
    };
    ApiserviceProvider.prototype.getRsvpEventList = function (eventID) {
        var eid = eventID;
        return this.https.get(this.rsvpeventlist + eid);
    };
    ApiserviceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */]])
    ], ApiserviceProvider);
    return ApiserviceProvider;
}());

//# sourceMappingURL=apiservice.js.map

/***/ }),

/***/ 47:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_apiservice_apiservice__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__committee_detail_committee_detail__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__qr_scan_qr_scan__ = __webpack_require__(110);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, popoverCtrl, events, ApiService, renderer, loading, alertCtrl) {
        this.navCtrl = navCtrl;
        this.popoverCtrl = popoverCtrl;
        this.events = events;
        this.ApiService = ApiService;
        this.renderer = renderer;
        this.loading = loading;
        this.alertCtrl = alertCtrl;
        this.homeSegment = "home";
        this.committeeinfo = "member";
        this.committeRefresh(0);
        this.showTotalMember();
        this.showTotalNonMember();
        this.showTotalLocalGuest();
        this.showTotalEvent();
        this.showTotalOverseaGuest();
        this.showTotalDiamondSponsor();
        this.showTotalPlatinumSponsor();
        this.showTotalGoldSponsor();
        this.showTotalSilverSponsor();
        this.showTotalSupportingSponsor();
        this.chooseEventToScanRefresh(0);
    }
    /*#region Committee*/
    HomePage.prototype.committeRefresh = function (refresher) {
        var _this = this;
        //Member
        this.ApiService.getAllMember()
            .subscribe(function (res) {
            var getMember = res.json();
            _this.memberitems = getMember;
            _this.initializeMember();
            if (refresher != 0) {
                refresher.complete();
            }
        });
        //localguest
        this.ApiService.getAllLocalGuest()
            .subscribe(function (res) {
            var getLocalGuest = res.json();
            _this.localguestitems = getLocalGuest;
            _this.initializeLocalGuest();
            if (refresher != 0) {
                refresher.complete();
            }
        });
        //overseaguest
        this.ApiService.getAllOverseaGuest()
            .subscribe(function (res) {
            var getOverseaGuest = res.json();
            _this.overseaguestitems = getOverseaGuest;
            _this.initializeOverseaGuest();
            if (refresher != 0) {
                refresher.complete();
            }
        });
        //NonMember
        this.ApiService.getAllNonMember()
            .subscribe(function (res) {
            var getNonMember = res.json();
            _this.nonmemberitems = getNonMember;
            _this.initializeNonMember();
            if (refresher != 0) {
                refresher.complete();
            }
        });
    };
    HomePage.prototype.searchMember = function (m) {
        this.initializeMember();
        var serVal = m.target.value;
        if (serVal && serVal.trim() != '') {
            this.showMember = this.showMember.filter(function (getmember) {
                return (getmember.EnglishName.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
            });
            this.renderer.invokeElementMethod(m.target, 'blur');
        }
    };
    HomePage.prototype.initializeMember = function () {
        this.showMember = this.memberitems;
    };
    HomePage.prototype.searchLocalGuest = function (lg) {
        this.initializeLocalGuest();
        var serVal = lg.target.value;
        if (serVal && serVal.trim() != '') {
            this.showLocalGuest = this.showLocalGuest.filter(function (getlocalguest) {
                return (getlocalguest.EnglishName.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
            });
            this.renderer.invokeElementMethod(lg.target, 'blur');
        }
    };
    HomePage.prototype.initializeLocalGuest = function () {
        this.showLocalGuest = this.localguestitems;
    };
    HomePage.prototype.searchOverseaGuest = function (og) {
        this.initializeOverseaGuest();
        var serVal = og.target.value;
        if (serVal && serVal.trim() != '') {
            this.showOverseaGuest = this.showOverseaGuest.filter(function (getoverseaguest) {
                return (getoverseaguest.EnglishName.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
            });
            this.renderer.invokeElementMethod(og.target, 'blur');
        }
    };
    HomePage.prototype.initializeOverseaGuest = function () {
        this.showOverseaGuest = this.overseaguestitems;
    };
    HomePage.prototype.searchNonMember = function (nm) {
        this.initializeNonMember();
        var serVal = nm.target.value;
        if (serVal && serVal.trim() != '') {
            this.showNonMember = this.showNonMember.filter(function (getnonmember) {
                return (getnonmember.EnglishName.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
            });
            this.renderer.invokeElementMethod(nm.target, 'blur');
        }
    };
    HomePage.prototype.initializeNonMember = function () {
        this.showNonMember = this.nonmemberitems;
    };
    HomePage.prototype.showSpecificUser = function (userid) {
        var _this = this;
        this.ApiService.getUser(userid).subscribe(function (res) {
            var getSpecificUser = res.json();
            _this.showSpecUser = getSpecificUser;
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__committee_detail_committee_detail__["a" /* CommitteeDetailPage */], {
                data: userid
            });
        });
        /*#endregion */
    };
    /*#region Admin Dashboard (Home)*/
    HomePage.prototype.showTotalMember = function () {
        var _this = this;
        this.ApiService.getTotalMember()
            .subscribe(function (res) {
            var member = res.json();
            _this.totalmember = member;
        });
    };
    HomePage.prototype.showTotalNonMember = function () {
        var _this = this;
        this.ApiService.getTotalNonMember()
            .subscribe(function (res) {
            var nonmember = res.json();
            _this.totalnonmember = nonmember;
        });
    };
    HomePage.prototype.showTotalLocalGuest = function () {
        var _this = this;
        this.ApiService.getLocalGuest()
            .subscribe(function (res) {
            var localguest = res.json();
            _this.totallocalguest = localguest;
        });
    };
    HomePage.prototype.showTotalOverseaGuest = function () {
        var _this = this;
        this.ApiService.getOverseaGuest()
            .subscribe(function (res) {
            var overseaguest = res.json();
            _this.totaloverseaguest = overseaguest;
        });
    };
    HomePage.prototype.showTotalEvent = function () {
        var _this = this;
        this.ApiService.getTotalEvent()
            .subscribe(function (res) {
            var eventt = res.json();
            _this.totalevent = eventt;
        });
    };
    HomePage.prototype.showTotalDiamondSponsor = function () {
        var _this = this;
        this.ApiService.getDiamondSponsor()
            .subscribe(function (res) {
            var diamondsponsor = res.json();
            _this.totaldiamondsponsor = diamondsponsor;
        });
    };
    HomePage.prototype.showTotalPlatinumSponsor = function () {
        var _this = this;
        this.ApiService.getPlatinumSponsor()
            .subscribe(function (res) {
            var platinumsponsor = res.json();
            _this.totalplatinumsponsor = platinumsponsor;
        });
    };
    HomePage.prototype.showTotalGoldSponsor = function () {
        var _this = this;
        this.ApiService.getGoldSponsor()
            .subscribe(function (res) {
            var goldsponsor = res.json();
            _this.totalgoldsponsor = goldsponsor;
        });
    };
    HomePage.prototype.showTotalSilverSponsor = function () {
        var _this = this;
        this.ApiService.getSilverSponsor()
            .subscribe(function (res) {
            var silversponsor = res.json();
            _this.totalsilversponsor = silversponsor;
        });
    };
    HomePage.prototype.showTotalSupportingSponsor = function () {
        var _this = this;
        this.ApiService.getSupportingSponsor()
            .subscribe(function (res) {
            var supportingsponsor = res.json();
            _this.totalsupportingsponsor = supportingsponsor;
        });
    };
    /*#endregion */
    HomePage.prototype.showScanAlert = function () {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            title: 'Alert',
                            subTitle: 'Scanning Info',
                            message: 'Please select the following event to scan the attendance.',
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.chooseEventToScanRefresh = function (refresher) {
        var _this = this;
        this.ApiService.getEvent().subscribe(function (res) {
            _this.items = res.json();
            _this.initializeEvent();
            if (refresher != 0) {
                refresher.complete();
            }
        });
    };
    HomePage.prototype.getEventTitle = function (evt) {
        this.initializeEvent();
        var serVal = evt.target.value;
        if (serVal && serVal.trim() != '') {
            this.eventlisting = this.eventlisting.filter(function (getevent) {
                return (getevent.EventTitle.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
            });
            this.renderer.invokeElementMethod(evt.target, 'blur');
        }
    };
    HomePage.prototype.initializeEvent = function () {
        this.eventlisting = this.items;
    };
    HomePage.prototype.gotoQRDetail = function (EventID) {
        var _this = this;
        this.ApiService.getSpecificEvent(EventID).subscribe(function (res) {
            var getSpecificEvent = res.json();
            _this.showSpecificEvent = getSpecificEvent;
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__qr_scan_qr_scan__["a" /* QrScanPage */], {
                data: EventID
            });
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\home\home.html"*/'<ion-header>\n  <ion-navbar>\n\n    <ion-title *ngIf="userisnotsignin">Welcome To ACCCIM</ion-title>\n\n    <ion-buttons right>\n\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n\n  <div [ngSwitch]="homeSegment">\n    <div *ngSwitchCase="\'home\'">\n      <h3>Total Registration</h3>\n      <ion-row>\n        <ion-col col-6>\n          <ion-card style=" background: linear-gradient(120deg, #e04718 0%, #ffb92d 100%);">\n            <ion-grid text-center>\n                <ion-row style="margin-top: 5px;">\n            <ion-col col-12> \n                <h2 style="margin-top: 8px;">Member</h2>         \n              \n                <p style="font-size:20px;">{{totalmember}}</p>   \n              </ion-col>   \n                              \n          </ion-row>\n            </ion-grid>             \n        </ion-card>\n        </ion-col>\n        <ion-col col-6>\n          <ion-card style=" background: linear-gradient(120deg, #e04718 0%, #ffb92d 100%);">\n            <ion-grid text-center>\n                <ion-row style="margin-top: 5px;">\n            <ion-col col-12> \n                <h2 style="margin-top: 8px;">Non-Member</h2>         \n              \n                <p style="font-size:20px;">{{totalnonmember}}</p>   \n              </ion-col>   \n                              \n          </ion-row>\n            </ion-grid>             \n        </ion-card>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col col-6>\n          <ion-card style=" background: linear-gradient(120deg, #e04718 0%, #ffb92d 100%);">\n            <ion-grid text-center>\n                <ion-row style="margin-top: 5px;">\n            <ion-col col-12> \n                <h2 style="margin-top: 8px;">Local Guest</h2>         \n              \n                <p style="font-size:20px;">{{totallocalguest}}</p>   \n              </ion-col>   \n                              \n          </ion-row>\n            </ion-grid>             \n        </ion-card>\n        </ion-col>\n        <ion-col col-6>\n          <ion-card style=" background: linear-gradient(120deg, #e04718 0%, #ffb92d 100%);">\n            <ion-grid text-center>\n                <ion-row style="margin-top: 5px;">\n            <ion-col col-12> \n                <h2 style="margin-top: 8px;">Oversea Guest</h2>         \n              \n                <p style="font-size:20px;">{{totaloverseaguest}}</p>   \n              </ion-col>   \n                              \n          </ion-row>\n            </ion-grid>             \n        </ion-card>\n        </ion-col>\n      </ion-row>\n\n      <h3>Total Event</h3>\n      <ion-row>\n        <ion-col col-2></ion-col>\n        <ion-col col-8>\n            <ion-card style=" background: linear-gradient(120deg, #e04718 0%, #ffb92d 100%);">\n                <ion-grid text-center>\n                    <ion-row style="margin-top: 5px;">\n                <ion-col col-12> \n                    <h2 style="margin-top: 8px;">Event</h2>       \n                  \n                    <p style="font-size:20px;">{{totalevent}}</p>   \n                  </ion-col>   \n                                  \n              </ion-row>\n                </ion-grid>             \n            </ion-card>\n        </ion-col>\n        <ion-col col-2></ion-col>\n      </ion-row>\n\n      <h3>Total Advertisement</h3>\n      <ion-row>\n        <ion-col col-3></ion-col>\n        <ion-col col-6>\n            <ion-card style=" background: linear-gradient(120deg, #e04718 0%, #ffb92d 100%);">\n                <ion-grid text-center>\n                    <ion-row style="margin-top: 5px;">\n                <ion-col col-12> \n                    <h2 style="margin-top: 8px;">Diamond Sponsor</h2>         \n                  \n                    <p style="font-size:20px;">{{totaldiamondsponsor}}</p>   \n                  </ion-col>   \n                                  \n              </ion-row>\n                </ion-grid>             \n            </ion-card>\n        </ion-col>\n        <ion-col col-3></ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6>\n            <ion-card style=" background: linear-gradient(120deg, #e04718 0%, #ffb92d 100%);">\n                <ion-grid text-center>\n                    <ion-row style="margin-top: 5px;">\n                <ion-col col-12> \n                    <h2 style="margin-top: 8px;">Platinum <br/> Sponsor</h2>         \n                  \n                    <p style="font-size:20px;">{{totalplatinumsponsor}}</p>   \n                  </ion-col>   \n                                  \n              </ion-row>\n                </ion-grid>             \n            </ion-card>\n        </ion-col>\n        <ion-col col-6>\n            <ion-card style=" background: linear-gradient(120deg, #e04718 0%, #ffb92d 100%);">\n                <ion-grid text-center>\n                    <ion-row style="margin-top: 5px;">\n                <ion-col col-12> \n                    <h2 style="margin-top: 8px;">Gold <br/> Sponsor</h2>         \n                  \n                    <p style="font-size:20px;">{{totalgoldsponsor}}</p>   \n                  </ion-col>   \n                                  \n              </ion-row>\n                </ion-grid>             \n            </ion-card>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n          <ion-col col-6>\n              <ion-card style=" background: linear-gradient(120deg, #e04718 0%, #ffb92d 100%);">\n                  <ion-grid text-center>\n                      <ion-row style="margin-top: 5px;">\n                  <ion-col col-12> \n                      <h2 style="margin-top: 8px;">Silver <br/> Sponsor</h2>         \n                    \n                      <p style="font-size:20px;">{{totalsilversponsor}}</p>   \n                    </ion-col>   \n                                    \n                </ion-row>\n                  </ion-grid>             \n              </ion-card>\n          </ion-col>\n          <ion-col col-6>\n              <ion-card style=" background: linear-gradient(120deg, #e04718 0%, #ffb92d 100%);">\n                  <ion-grid text-center>\n                      <ion-row style="margin-top: 5px;">\n                  <ion-col col-12> \n                      <h2 style="margin-top: 8px;">Supporting <br/> Sponsor</h2>         \n                    \n                      <p style="font-size:20px;">{{totalsupportingsponsor}}</p>   \n                    </ion-col>   \n                                    \n                </ion-row>\n                  </ion-grid>             \n              </ion-card>\n          </ion-col>\n        </ion-row>\n    </div>\n\n    <div *ngSwitchCase="\'committee\'">\n        <div>\n            <ion-refresher (ionRefresh)="committeRefresh($event);">\n              <ion-refresher-content></ion-refresher-content>\n            </ion-refresher>\n          </div>\n      <ion-segment [(ngModel)]="committeeinfo">\n\n        <ion-segment-button value="member">\n          <p class="item-title" style="color:#ED6D10;">Member</p>\n        </ion-segment-button>\n\n        <ion-segment-button value="overseaguest">\n          <p class="item-title" style="color:#ED6D10;">Ovrs Guest</p>\n\n        </ion-segment-button>\n\n        <ion-segment-button value="localguest">\n          <p class="item-title" style="color:#ED6D10;">Local Guest</p>\n\n        </ion-segment-button>\n\n        <ion-segment-button value="nonmember">\n          <p class="item-title" style="color:#ED6D10;">Non-Member</p>\n\n        </ion-segment-button>\n\n\n\n      </ion-segment>\n\n      <div [ngSwitch]="committeeinfo">\n        <div *ngSwitchCase="\'member\'">\n            <ion-searchbar (ionInput)="searchMember($event);" [debounce]="500" placeholder="Search By Name Only">\n              </ion-searchbar>\n            <ion-card *ngFor="let getmember of showMember">\n                <ion-grid (click)="showSpecificUser(getmember.id);">\n                    <ion-row style="margin-top: 5px;">\n                    \n    \n                <ion-col col-10> \n                    <h3 style="margin-top: 8px;">{{getmember.EventRegID}}</h3>         \n                    <h3 style="margin-top: 8px;">{{getmember.EnglishName}}</h3>\n                    <p>{{getmember.UserType}}</p>   \n                  </ion-col>    \n                              \n              </ion-row>\n                </ion-grid>             \n            </ion-card>\n        </div>\n      </div>\n\n        <div [ngSwitch]="committeeinfo">\n            <div *ngSwitchCase="\'localguest\'">\n                <ion-searchbar (ionInput)="searchLocalGuest($event);" [debounce]="500" placeholder="Search By Name Only">\n                  </ion-searchbar>\n                <ion-card *ngFor="let getlocalguest of showLocalGuest">\n                    <ion-grid (click)="showSpecificUser(getlocalguest.id);">\n                        <ion-row style="margin-top: 5px;">\n                        \n        \n                    <ion-col col-10> \n                        <h3 style="margin-top: 8px;">{{getlocalguest.EventRegID}}</h3>         \n                        <h3 style="margin-top: 8px;">{{getlocalguest.EnglishName}}</h3>\n                        <p>{{getlocalguest.UserType}}</p>   \n                      </ion-col>   \n                                         \n                  </ion-row>\n                    </ion-grid>             \n                </ion-card>\n            </div>\n          </div>\n\n          <div [ngSwitch]="committeeinfo">\n              <div *ngSwitchCase="\'overseaguest\'">\n                  <ion-searchbar (ionInput)="searchOverseaGuest($event);" [debounce]="500" placeholder="Search By Name Only">\n                    </ion-searchbar>\n                  <ion-card *ngFor="let getoverseaguest of showOverseaGuest">\n                      <ion-grid (click)="showSpecificUser(getoverseaguest.id);">\n                          <ion-row style="margin-top: 5px;">\n                          \n          \n                      <ion-col col-12> \n                          <h3 style="margin-top: 8px;">{{getoverseaguest.EventRegID}}</h3>         \n                          <h3 style="margin-top: 8px;">{{getoverseaguest.EnglishName}}</h3>\n                          <p>{{getoverseaguest.UserType}}</p>   \n                        </ion-col>   \n                                         \n                    </ion-row>\n                      </ion-grid>             \n                  </ion-card>\n              </div>\n            </div>\n\n            <div [ngSwitch]="committeeinfo">\n                <div *ngSwitchCase="\'nonmember\'">\n                    <ion-searchbar (ionInput)="searchNonMember($event);" [debounce]="500" placeholder="Search By Name Only">\n                      </ion-searchbar>\n                    <ion-card *ngFor="let getnonmember of showNonMember">\n                        <ion-grid (click)="showSpecificUser(getnonmember.id);">\n                            <ion-row style="margin-top: 5px;">\n                            \n            \n                        <ion-col col-10> \n                            <h3 style="margin-top: 8px;">{{getnonmember.EventRegID}}</h3>         \n                            <h3 style="margin-top: 8px;">{{getnonmember.EnglishName}}</h3>\n                            <p>{{getnonmember.UserType}}</p>   \n                          </ion-col>   \n                                       \n                      </ion-row>\n                        </ion-grid>  \n                                  \n                    </ion-card>\n                </div>\n              </div>\n    </div>\n\n\n\n\n\n\n    <div *ngSwitchCase="\'attendance\'">\n\n    </div>\n\n    <div *ngSwitchCase="\'scan\'">\n        <ion-searchbar (ionInput)="getEventTitle($event);" [debounce]="500" placeholder="Search By Event Title Only">\n          </ion-searchbar>\n        <div *ngFor="let item of eventlisting" style="margin-top:30px;">\n            <div *ngIf="item">\n              <ion-card>\n                <ion-grid (click)="gotoQRDetail(item.EventID);">\n                  <ion-row>\n                    <ion-col col-12>\n\n                      <div *ngIf="item.EventImage">\n                        <img style="width: 100%; height: 100%; border-radius: 9px"\n                          src="http://admin.acccim-registration.org.my{{item.EventImage}}">\n                      </div>\n                      <div *ngIf="!item.EventImage">\n                        <img style="width: 100%; height: 100%; border-radius: 9px"\n                          src="../../assets/imgs/ACCIMlogo.png">\n                      </div>\n\n\n\n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n\n                    <ion-col col-12>\n                      <p style="margin-top: 8px;" text-center><b>{{item.EventTitle}}</b></p>\n\n\n\n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col col-2>\n                      <label>\n                        <ion-icon name="calendar" style="font-size:30px; margin-right:20px;"></ion-icon>\n                      </label>\n                    </ion-col>\n                    <ion-col col-10>\n                      <label class="product-price">{{item.StartDate | date: "yyyy/MM/dd"}}</label>\n                      <label class="product-price"> {{item.StartTime}}</label>\n                    </ion-col>\n\n                    <ion-col col-2>\n                      <label>\n                        <ion-icon name="person" style="font-size:30px; margin-right:20px;"></ion-icon>\n                      </label>\n                    </ion-col>\n                    <ion-col col-10>\n                      <label><i>Available For :</i></label>\n                      <div *ngFor="let gett of item.MemberAvailability_tb">\n                        <label>{{gett.MemberType}}</label>\n                      </div>\n                    </ion-col>\n                  </ion-row>\n                </ion-grid>\n              </ion-card>\n            </div>\n\n\n          </div>\n    </div>\n\n  </div>\n\n\n\n\n</ion-content>\n\n\n\n<ion-footer>\n\n  <ion-toolbar class="footer-color">\n\n    <ion-segment [(ngModel)]="homeSegment">\n      <ion-segment-button value="home">\n        <ion-icon name="home" style="margin-top: -50px !important; ">\n        </ion-icon>\n\n        <div style="margin-top: -40px !important; ">\n          <p class="item-title" style="color:#ED6D10;">Home</p>\n        </div>\n      </ion-segment-button>\n\n\n\n      <ion-segment-button value="committee">\n        <ion-icon name="md-contacts" style="margin-top: -50px !important; "></ion-icon>\n        <div style="margin-top: -40px !important; ">\n          <p class="item-title" style="color:#ED6D10;">Committee</p>\n        </div>\n      </ion-segment-button>\n      <ion-segment-button value="attendance">\n        <ion-icon name="md-checkbox-outline" style="margin-top: -50px !important; "></ion-icon>\n        <div style="margin-top: -40px !important; ">\n          <p class="item-title" style="color:#ED6D10;">Attendance</p>\n        </div>\n      </ion-segment-button>\n\n      <ion-segment-button value="scan" (click)="showScanAlert();">\n        <ion-icon name="md-qr-scanner" style="margin-top: -50px !important; "></ion-icon>\n        <div style="margin-top: -40px !important; ">\n          <p class="item-title" style="color:#ED6D10;">Scan</p>\n        </div>\n      </ion-segment-button>\n\n\n    </ion-segment>\n  </ion-toolbar>\n</ion-footer>'/*ion-inline-end:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_2__providers_apiservice_apiservice__["a" /* ApiserviceProvider */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["V" /* Renderer */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 53:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductSizePopOverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProductSizePopOverPage = /** @class */ (function () {
    function ProductSizePopOverPage(navCtrl, navParams, viewController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewController = viewController;
    }
    ProductSizePopOverPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProductSizePopOverPage');
    };
    ProductSizePopOverPage.prototype.closePopover = function () {
        this.viewController.dismiss();
    };
    ProductSizePopOverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-product-size-pop-over',template:/*ion-inline-start:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\product-size-pop-over\product-size-pop-over.html"*/'<ion-list no-margin no-lines>\n  <ion-item>\n    <button ion-item no-lines (click)="closePopover()">\n      XS\n    </button>\n  </ion-item>\n\n  <ion-item>\n    <button ion-item no-lines (click)="closePopover()">\n      S\n    </button>\n  </ion-item>\n\n  <ion-item>\n    <button ion-item no-lines (click)="closePopover()">\n      M\n    </button>\n  </ion-item>\n\n  <ion-item>\n    <button class="out-of-stock" ion-item no-lines (click)="closePopover()">\n      L - Out of stock\n    </button>\n  </ion-item>\n\n  <ion-item>\n    <button class="out-of-stock" ion-item no-lines (click)="closePopover()">\n      XL - Out of stock\n    </button>\n  </ion-item>\n</ion-list>'/*ion-inline-end:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\product-size-pop-over\product-size-pop-over.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */]])
    ], ProductSizePopOverPage);
    return ProductSizePopOverPage;
}());

//# sourceMappingURL=product-size-pop-over.js.map

/***/ }),

/***/ 84:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SavedItemsPage; });
/* unused harmony export Item */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__product_colour_pop_over_product_colour_pop_over__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__product_size_pop_over_product_size_pop_over__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SavedItemsPage = /** @class */ (function () {
    function SavedItemsPage(navCtrl, navParams, popoverCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.savedItems = [];
        this.itemsCount = 0;
        this.getExampleItems();
    }
    SavedItemsPage.prototype.getExampleItems = function () {
        // Discounted item
        var discountedItem = new Item();
        discountedItem.name = "Pull&Bear Denim Jacket In Black";
        discountedItem.price = 40;
        discountedItem.hasDiscount = true;
        discountedItem.discountedPrice = 30;
        discountedItem.picture = "https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-1.jpg";
        this.savedItems.push(discountedItem);
        // Item where the size and colour have been selected
        var setSizeAndColourItem = new Item();
        setSizeAndColourItem.name = "ASOS Embroidered Mini Dress";
        setSizeAndColourItem.price = 109;
        setSizeAndColourItem.sizeAndColourSelected = true;
        setSizeAndColourItem.size = "M";
        setSizeAndColourItem.colour = "Black";
        setSizeAndColourItem.canAddToBag = true;
        setSizeAndColourItem.picture = "https://res.cloudinary.com/cediim8/image/upload/v1523415017/women-product-8.jpg";
        this.savedItems.push(setSizeAndColourItem);
        // Normal item without discount and size and colour not set yet
        var item = new Item();
        item.name = "Lee Sherpa Rider Denim Jacket";
        item.price = 218;
        item.hasDiscount = false;
        item.picture = "https://res.cloudinary.com/cediim8/image/upload/v1523344614/product-4.jpg";
        item.size = "M";
        item.colour = "Black";
        this.savedItems.push(item);
        this.itemsCount = this.savedItems.length;
    };
    SavedItemsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SavedItemsPage');
    };
    SavedItemsPage.prototype.editItem = function (item) {
        item.isInEditMode = !item.isInEditMode;
    };
    SavedItemsPage.prototype.colourPopOver = function (myEvent) {
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_2__product_colour_pop_over_product_colour_pop_over__["a" /* ProductColourPopOverPage */]);
        popover.present({
            ev: myEvent
        });
    };
    SavedItemsPage.prototype.sizePopOver = function (myEvent) {
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_3__product_size_pop_over_product_size_pop_over__["a" /* ProductSizePopOverPage */]);
        popover.present({
            ev: myEvent
        });
    };
    SavedItemsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-saved-items',template:/*ion-inline-start:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\saved-items\saved-items.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-buttons left>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n\n    <ion-title>\n      <ion-row>\n        <p class="title">SAVED ITEMS</p>\n        <p class="items-count">({{ itemsCount }} items)</p>\n      </ion-row>\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-list>\n    <ion-item no-lines class="saved-item" *ngFor="let item of savedItems">\n      <img item-start src="{{ item.picture }}">\n\n      <ion-col *ngIf="!item.isInEditMode">\n        <ion-item class="name-bin" no-lines>\n          <p item-start>{{ item.name }}</p>\n\n          <button item-end ion-button clear icon-only>\n            <ion-icon name="ios-trash-outline"></ion-icon>\n          </button>\n        </ion-item>\n\n        <ion-row *ngIf="!item.hasDiscount">\n          <p class="actual-price">${{ item.price }}</p>\n        </ion-row>\n\n        <ion-row *ngIf="item.hasDiscount">\n          <p class="discounted-price">${{ item.discountedPrice }}</p>\n          <p class="non-discounted-price">${{ item.price }}</p>\n        </ion-row>\n\n        <p class="size-colour" *ngIf="item.sizeAndColourSelected">{{ item.size }} / {{ item.colour }}</p>\n        <p class="select-size-colour" *ngIf="!item.sizeAndColourSelected">SELECT EDIT FOR SIZE AND COLOUR</p>\n\n        <ion-item no-lines>\n          <button class="edit-button" item-start outline ion-button (click)="editItem(item)">\n            <p>EDIT</p>\n          </button>\n\n          <button *ngIf="!item.canAddToBag" class="add-to-bag-button" outline ion-button disabled>\n            <p>ADD TO BAG</p>\n          </button>\n\n          <button *ngIf="item.canAddToBag" class="add-to-bag-button" outline ion-button>\n            <p>ADD TO BAG</p>\n          </button>\n        </ion-item>\n      </ion-col>\n\n      <ion-col *ngIf="item.isInEditMode">\n        <ion-item no-lines class="colour-size" (click)="colourPopOver($event)">\n          <p item-start>COLOUR</p>\n          <ion-icon item-end name="ios-arrow-down"></ion-icon>\n        </ion-item>\n\n        <ion-item no-lines class="colour-size" (click)="sizePopOver($event)">\n          <p item-start>SIZE</p>\n          <ion-icon item-end name="ios-arrow-down"></ion-icon>\n        </ion-item>\n\n        <ion-row>\n          <ion-col text-center (click)="editItem(item)">\n            <button class="cancel-button" ion-button>\n              <p>CANCEL</p>\n            </button>\n          </ion-col>\n\n          <ion-col text-center (click)="editItem(item)">\n            <button class="apply-button" ion-button>\n              <p>APPLY</p>\n            </button>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-item>\n  </ion-list>\n</ion-content>'/*ion-inline-end:"D:\DRG Projects\ACCCIM Admin\asos-admin\src\pages\saved-items\saved-items.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* PopoverController */]])
    ], SavedItemsPage);
    return SavedItemsPage;
}());

var Item = /** @class */ (function () {
    function Item() {
    }
    return Item;
}());

//# sourceMappingURL=saved-items.js.map

/***/ })

},[218]);
//# sourceMappingURL=main.js.map